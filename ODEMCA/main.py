import matplotlib.pyplot as plt
import numpy as np
import time
from numpy.matlib import repmat
from scipy.integrate import solve_ivp
from scipy.signal import savgol_filter

num_chromphers = 1
maxTime = 5e-5
kexc = 1e6
kisc = 1e9
kiscR = 1e5
tauFL = 500e-12
kst = 1e12
ktt = 1e9


def RunSim(
            nMax = 10,
            iterNum = 500,
            tol = 1.0e-6,
            timestep = 2e-8):

    endTime = maxTime

    qMax = 100

    timestep = endTime / qMax

    kf = np.zeros((nMax + 1, 1))
    kf.fill(kexc * kisc * tauFL)

    P = np.zeros((1, nMax + 1))
    P[0, 0] = 1
    tmp = repmat(np.append([0.99], np.linspace(1e-6, 1e-7, nMax)), qMax + 1, 1)

    P = np.append(P, tmp, axis=0)
    P = np.append(P, np.zeros((qMax + 2, 2)), axis=1)

    Pold = P.copy()[0:qMax + 2, 0:nMax + 1]

    b = np.zeros((qMax + 1, nMax + 1))

    theta = 0.5

    #err_Store = np.zeros((iterNum, qMax))

    for q in range(0, qMax):
        iteration = 1
        err = [100000]

        while iteration <= iterNum and max(err) > tol:
            for n in range(0, nMax + 1):
                if n - 1 < 0:
                    tmp_b = 0.0
                    tmp_P = 0.0
                else:
                    tmp_b = kf[n] * P[q, n - 1]
                    tmp_P = kf[n] * P[q + 1, n - 1]

                b_part1 = P[q, n] * ((1 / timestep) - (1 - theta) * (kf[n] + n * kiscR + n * (n - 1) * (ktt * 0.5)))
                b_part2 = (1 - theta) * (
                            tmp_b + kiscR * (n + 1) * P[q, n + 1] + (ktt * 0.5) * (n + 2) * (n + 1) * P[q, n + 2])
                b[q, n] = b_part1 + b_part2

                P_numerator = theta * (tmp_P + kiscR * (n + 1) * P[q + 1, n + 1] + (ktt * 0.5) * (n + 2) * (n + 1) * P[
                    q + 1, n + 2]) + b[q, n]
                P_denominator = (1 / timestep) + theta * (kf[n] + n * kiscR + n * (n - 1) * (ktt * 0.5))

                P[q + 1, n] = P_numerator / P_denominator

            err = abs((P[q + 1, 0:nMax + 1] - Pold[q + 1, :]) / P[q + 1, 0: nMax + 1])
            Pold = P.copy()[0:qMax + 2, 0: nMax + 1]

            #err_Store[iteration - 1, q] = max(err)
            iteration += 1

    Pfinal = P[0:qMax, 0: nMax]

    t = np.array(range(0, qMax)) * timestep
    t = t

    I = np.zeros((1, qMax))

    for n in range(0, nMax):
        I = I + (Pfinal[:, n] / (1 + (n * kst * tauFL)))

    return [t, I[0], P]

def EvalEq(t, y):
    n00 = y[0]
    n10 = y[1]
    n20 = y[2]
    n01 = y[3]
    n02 = y[4]
    n11 = y[5]

    dn00 = -kexc * n00 + (1 / tauFL) * n10 + kiscR * n01 + ktt * n02
    dn10 = -(kexc + kisc + (1 / tauFL)) * n10 + kexc * n00 + (1 / tauFL) * n20 + kiscR * n11
    dn20 = -((1 / tauFL) + kisc) * n20 + kexc * n20
    dn01 = -(kexc + kiscR) * n01 + ((1 / tauFL) + kst) * n11 + kiscR * n02 + kisc * n10
    dn02 = -(kiscR + ktt) * n02 + kisc * n11
    dn11 = -(kiscR + (1 / tauFL) + kst + kisc) * n11 + kexc * n01
    dhv = (1 / tauFL) * (n10 + n20 + n11)

    return [dn00, dn10, dn20, dn01, dn02, dn11, dhv]

t = time.time()
sol = solve_ivp(EvalEq, [0, maxTime], [num_chromphers, 0, 0, 0, 0, 0, 0])
elapsed = time.time() - t

# implent def I(t) as tachyia
P = np.array([sol.y[0,:], sol.y[3,:], sol.y[4,:]])
P = np.transpose(P)

I = P[:,0]

# loop over the number of states

for n in range(1,3):
    I = np.sum([I ,P[:,n]/(1 + (n*kst*tauFL))],axis=0)




#plt.plot(np.gradient(sol.y[6, :]))
#plt.show()

#plt.plot(,  81, 1), linewidth =0.1)
#plt.show()



t = sol.t[10:-50]
# we shouldn't be comparing the dhv to Tachiya we need build Tachyia's def of I(t) and compare to Tachyia
I = savgol_filter(np.gradient(sol.y[6, :])[10:-50], 201, 1) 


I = I - np.min(I)
I = I / np.max(I)

[tach_t, tach_I, _] = RunSim()

tach_I = tach_I - np.min(tach_I)
tach_I = tach_I / np.max(tach_I)

plt.plot(tach_t, tach_I)
plt.plot(t, I)

plt.savefig("test.svg", format="svg")