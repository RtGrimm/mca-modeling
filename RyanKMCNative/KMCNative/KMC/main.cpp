#include <utility>
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <random>
#include <cmath>
#include <tbb/tbb.h>
#include <fstream>
#include <tuple>

using VecD = std::vector<double>;

template<class T>
int DuplicateCount(const std::vector<T> &xs, const T &x) {
    return std::accumulate(std::begin(xs), std::end(xs), 0,
                           [&](int acc, const T &y) { return acc + (x == y ? 1 : 0); }) - 1;
}

class ReactionElement {
public:
    int N = 0;
    std::vector<std::tuple<double, int>> History;
    std::string Name;

    ReactionElement(std::string name, int n = 0) : N(n), Name(std::move(name)) {}

    void Update(double t, int n) {
        History.emplace_back(t, n);
        N = n;
    }

    bool operator==(const ReactionElement &rhs) const {
        return Name == rhs.Name;
    }

    bool operator!=(const ReactionElement &rhs) const {
        return !(rhs == *this);
    }
};

class Reaction {
public:
    double Rate;
    std::vector<ReactionElement*> Reactants;
    std::vector<ReactionElement*> Products;

    Reaction(double Rate, std::vector<ReactionElement*> Reactants, std::vector<ReactionElement*> Products)
            : Rate(Rate), Reactants(std::move(Reactants)), Products(std::move(Products)) {}


    double PopProduct() {
        return std::accumulate(std::begin(Reactants), std::end(Reactants), 1,
                               [&](int acc, ReactionElement* reactant) {
                                   auto dc = DuplicateCount(Reactants, reactant);
                                   auto x = (reactant->N - dc) * (1.0 / (static_cast<double>(dc) + 1.0));
                                   return acc * x;
                               });
    }

    void Run(double t) {
        std::for_each(std::begin(Reactants), std::end(Reactants), [&] (ReactionElement* element) {
           element->Update(t, element->N - 1);
        });

        std::for_each(std::begin(Products), std::end(Products), [&] (ReactionElement* element) {
            element->Update(t, element->N + 1);
        });
    }
};

class Simulation {
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<double> dis;
public:
    std::vector<Reaction*> Reactions;
    double t = 0;

    Simulation(std::vector<Reaction*> Reactions) : Reactions(std::move(Reactions)), t(0) {
        gen = std::mt19937(rd());
        dis = std::uniform_real_distribution<double>(0, 1);
    }


    double Rand() {
        return dis(gen);
    }

    double DrawTime(double wtotal) {
        return std::abs( (-1.0 / wtotal) * std::log(Rand()));
    }


    void Run(double maxTime) {
        while(t < maxTime) {
            std::vector<double> rateList;

            double wtotal = 0;
            for(auto reaction : Reactions) {
                auto product = reaction->PopProduct();
                wtotal += product * reaction->Rate;
                rateList.push_back(wtotal);
            }

            auto r = Rand() * wtotal;

            Reaction* reaction = nullptr;

            auto i = 0;

            for(double rate : rateList) {
                if(rate > r) {
                    reaction = Reactions[i];
                    break;
                }

                i++;
            }

            if(reaction != nullptr)
                reaction->Run(t);


            auto dt = DrawTime(wtotal);
            t += dt;
        }
    }
};

void RunSim(double kexc, int i) {
    int num_chromphers = 12;

    double maxTime = 5e-5;
    double kisc = 1e9;
    double kiscR = 1e5;
    double tauFL = 500e-12;
    double kst = 5e8;
    double ktt = 1e4;
    double ksf = 1e9;

    std::vector<double> total;

    auto cycles = 10000;

    std::vector<std::vector<std::tuple<double, int>>> countList;
    countList.resize(cycles);

    tbb::parallel_for(0, cycles, [&] (int i ) {
        auto S0 = ReactionElement("S0", num_chromphers);
        auto S1 = ReactionElement("S1");
        auto T1 = ReactionElement("T1");
        auto hv = ReactionElement("Photons");


        auto sf = Reaction(ksf, {&S1, &S0}, {&T1, &T1});
        auto exc = Reaction(kexc, {&S0}, {&S1});
        auto isc = Reaction(kisc, {&S1}, {&T1});
        auto iscR = Reaction(kiscR, {&T1}, {&S0});
        auto fl = Reaction(1 / tauFL, {&S1}, {&S0, &hv});
        auto st = Reaction(kst, {&S1, &T1}, {&S0, &T1});
        auto tt = Reaction(ktt, {&T1, &T1}, {&S0, &S0});

        Simulation simulation({
                                      &exc, &isc, &iscR, &fl, &st, &tt, &sf
                              });

        simulation.Run(maxTime);

        countList[i] = std::move(hv.History);
    });

    std::cout << "Done";


    std::ofstream file("output" + std::to_string(i));


    for(auto& list : countList) {
        for(std::tuple<double, int> t : list) {
            file << std::get<0>(t)  << std::endl;
        }
    }

    file.close();
}

int main() {
    for(auto i = 4; i < 13; i++) {
        RunSim(pow(10, static_cast<double>(i)), i);
        std::cout << pow(10, static_cast<double>(i)) << std::endl;
    }

    return 0;
}