function [QDepths,Max_XYs,SS_XYs,FWHM_XYs,smoothedCts] = ...
    MCA_ClassPtCollector(...
    timeBasis,pulseTimeVec,maxTime,PhotonCts,phosphorescenceYesOrNo)
% D.J. Walwark 2019 - Function for parsing simulated MCA traces.
% % Requires simulation time parameters as a surrogate instrument response
% function.
%Right now it gets maximums and steady states (last 10%) as well as the FWHM
%It expects a pulseTime vector

%To get maxima from smoothed traces instead of raw (set true):
 % (This works best when time resolution is good!)
highSNRmode = 0;

%Insist on linear fitting less heartily if SNR is good.
        if highSNRmode == 1
        changeDCthresh =0.01;
        else
        changeDCthresh =0.08;
        end



%To enable (broken) plotting set to 1
debugMode =0;

%How much to increase smoothing by if multiple intersections are found
%in while-loop
whileSmoothAdditive = 0.0125;

%*************************************************************************
%Initialize smoothed matrix
smoothedCts=PhotonCts;

%get vector lengths
[timeBins,numTraces]=size(PhotonCts);

%initialize outputs matrices
FWHM_XYs= zeros(numTraces,2);
Max_XYs= FWHM_XYs;
SS_XYs= FWHM_XYs;

%The result vectors
FWHM_Xs = zeros(numTraces,1);
FWHM_Ys = FWHM_Xs;
Max_Ys = FWHM_Xs;
SS_Ys = FWHM_Xs;
Max_Xs = FWHM_Xs;
SS_Xs = FWHM_Xs;

%initialize some flag matrices
tooWeak_UseLinear = zeros(numTraces,1);
tooWeak_UseMedian = zeros(numTraces,1);

%Make vector of pulse-on-time proportions
onProportion = pulseTimeVec./maxTime;

%Make vector of pulse-end bin (floating point)
endBinsFloat = timeBins*onProportion;


%making integer/index of each pulse-end
endBins = floor(endBinsFloat);

%Locate bin of 90% pulse on-time completion (steady-state start)
ssStartBins = round(0.9*endBinsFloat);

%Locate bin of 90% pulse on-time completion
middleBins = round(0.5*endBinsFloat);

%Locate bin of 5% pulse on-time completion  (max estimate window end)
maxFindEndBins = round(0.05*endBinsFloat);
%Make sure there is at least a little bit of window to maxfind over!
maxFindEndBins(maxFindEndBins < 3) = 3;


%Built this conditional in but it isn't finished -
if phosphorescenceYesOrNo == 0
    
    for traceN =  numTraces: -1 :1
        
        %Light-weight processing and flag-setting
        
        %Debug plotting
        if debugMode == 1
            figure;
        end
        
        
        %code readability tmp variable assignations
        sStmpIndex = ssStartBins(traceN);
        endtmpIndex = endBins(traceN) ;
        maxFindRange = 1:maxFindEndBins(traceN);
        pulseRange = 1:  endtmpIndex;
        
        %Trace while pulse is on tmp assignation
        fitMeImOn=PhotonCts(pulseRange,traceN);
        
        %Make the system of equations matrix ('design matrix')
        Dmat=ones(endtmpIndex,2);
        Dmat(:,1)=timeBasis(pulseRange);
        
        %do linear regression (yes, this is 'fitting')
        coeffsTmp = (Dmat'*Dmat)^-1*Dmat'*fitMeImOn
        
        %Calculate vertical 'distance' traveled by trace via linear fit
        % (Just multiply the slope by the on-time)
        linearChangeOverPulseTime(traceN) = coeffsTmp(1) * pulseTimeVec(traceN);
        
        %Check the quotient of distance-change over signal level (fitted DC)
        changeOverDC = linearChangeOverPulseTime(traceN)/coeffsTmp(2)
        
        %Assign some things that will be overwritten if flags are set.
        %get maximums near beginning (first 5% of trace)
        [Max_Ys(traceN),maxIndices(traceN)]=max(PhotonCts(maxFindRange,traceN));
        Max_Xs(traceN) = timeBasis(maxIndices(traceN));
        %steady state intensity: median of final 10% of pulseTime
        SS_Ys(traceN) = median(PhotonCts(sStmpIndex:endtmpIndex,traceN));
        SS_Xs(traceN) = timeBasis(ssStartBins(traceN));
        
        
        
        %If the change is less than 5% of the signal level then we will use
        %the linear fit to classify it.
        if abs(changeOverDC) < changeDCthresh      %0.05
            %set flag
            tooWeak_UseLinear(traceN) = 1;
            %Assign linear regression values where appropriate
            Max_Ys(traceN) =  coeffsTmp(2);
            SS_Ys(traceN) = coeffsTmp(1)*timeBasis(endBins(traceN))+coeffsTmp(2);
            FWHM_Ys(traceN) = coeffsTmp(1)*timeBasis(middleBins(traceN))+coeffsTmp(2);
            Max_Xs(traceN) = timeBasis(1);
            SS_Xs(traceN) = timeBasis(endBins(traceN));
            FWHM_Xs(traceN) = nan;   %timeBasis(middleBins(traceN));
            %'smoothed' trace is linear regression
            smoothedCts(pulseRange,traceN) =...
                coeffsTmp(1)*timeBasis(pulseRange)+coeffsTmp(2);
            
        end % weakness flag conditional end
        
        %If the change is POSITIVE (and we have screened out
        %phosphorescence or finished writing this function to do so
        %etc) then there is no quenching and  max = min and fwhm-time
        %is steady state time (end). Within error, the line is FLAT.
        if changeOverDC > 0
            
            %set flag
            tooWeak_UseMedian(traceN) = 1;
            %Assign median to all values and time location to middle of
            %pulse. This makes it clear when plotting how it went down
            Max_Ys(traceN) =  median(fitMeImOn);
            SS_Ys(traceN) = Max_Ys(traceN);
            FWHM_Ys(traceN) = Max_Ys(traceN);
            Max_Xs(traceN) = timeBasis(middleBins(traceN));
            SS_Xs(traceN) = Max_Xs(traceN);
            FWHM_Xs(traceN) =  nan;  %Max_Xs(traceN);
            %'smoothed' trace is just median
            smoothedCts(pulseRange,traceN) = repmat(Max_Ys(traceN),endtmpIndex,1);
            
        end % weakness flag conditional end
        
        %Check flags to avoid useless processing
        if tooWeak_UseMedian(traceN) == 1 || tooWeak_UseLinear(traceN) == 1
            continue %Moves to next trace
        end
        
        
        % IF desired just get the maxima from the smoothed trace
            % (enable at top)
            if highSNRmode ==1
                [Max_Ys(traceN),maxIndices(traceN)]=max(smoothedCts(maxFindRange,traceN));
                Max_Xs(traceN) = timeBasis(maxIndices(traceN));
            end
                          
            %Get half-max height
            FWHM_Ys(traceN) = SS_Ys(traceN) + (Max_Ys(traceN) - SS_Ys(traceN))/2;
        
        
        % while-loop Initializations
        smthWndwProportion = 0.04 ;
        FWHM_TimeTMP = [0,0];
        
        % while-loop designed to increase smoothing until only one
        % intersection point is found.
        
        while length(FWHM_TimeTMP) > 1 && smthWndwProportion < 0.2 %|| length(FWHM_TimeTMP) < 1
            
            %smoothing window size in terms of pulseTime
            smthWndwProportion = smthWndwProportion + whileSmoothAdditive;
            
            %smoothing window size in bins
            PercentNumBins = round(smthWndwProportion*endBinsFloat);
            %Make sure there is at least a little bit of window to smooth over!
            PercentNumBins(PercentNumBins < 3) = 3;
            
            
            %Get smoothed photonCts for intersection calc
            smoothedCts(pulseRange,traceN) = ...
                smooth(PhotonCts(pulseRange,traceN),PercentNumBins(traceN),'sgolay',1);
            
            
            
%             % IF desired just get the maxima from the smoothed trace
%             % (enable at top)
%             if highSNRmode ==1
%                 [Max_Ys(traceN),maxIndices(traceN)]=max(smoothedCts(maxFindRange,traceN));
%                 Max_Xs(traceN) = timeBasis(maxIndices(traceN));
%             end
%                           
%             %Get half-max height
%             FWHM_Ys(traceN) = SS_Ys(traceN) + (Max_Ys(traceN) - SS_Ys(traceN))/2;
            
            %A catch for when the intersection will never be found (shrink
            %the target until it is within trace)
            while FWHM_Ys(traceN) > max(smoothedCts(pulseRange,traceN))
                FWHM_Ys(traceN) = FWHM_Ys(traceN) * 0.7;
            end
            
            %Make vectors of constant value (this is the y-value)
            FWHMIntersectorTmp = repmat(FWHM_Ys(traceN),timeBins,1);
            
%             traceN
            %Magic intersection matlab function: returns intersection coordinates
            % This thing is rather slow.
            [FWHM_TimeTMP, ~] = polyxpoly(timeBasis(pulseRange), FWHMIntersectorTmp(pulseRange), ...
                timeBasis(pulseRange),smoothedCts(pulseRange,traceN));
            
            
        end   %While end
        
        %Having met the while-loop condition, assign the desired
        %halfmax-time to the result vector.
        try
        FWHM_Xs(traceN) = FWHM_TimeTMP;
        catch
            FWHM_Xs(traceN) = nan;  disp('FWHM x-value location failure')
        end
        
        
        %Debug plotting - enable at start of function if desired
        if debugMode == 1
            hold on
            
            plot(timeBasis,PhotonCts(:,traceN));
            plot(timeBasis,smoothedCts(:,traceN),'LineWidth',1.5);
            scatter(timeBasis(maxIndices(traceN)),Max_Ys(traceN),50,'b','Filled');
            %                 yline(HalfMaxes(n));
            scatter(FWHM_Xs(traceN),FWHM_Ys(traceN),50,[0 0.7 0],'Filled');
            %                 xline(FWHM_Time);
            %scatter to start of steady state wtraceNow and calcd SS value
            scatter(timeBasis(ssStartBins(traceN)),SS_Ys(traceN),50,'k');
        end
        
        xlim([-1e-7 max(timeBasis)/1.95])
        
        
    end  %numtraces for-loop end
    
    
else  %phosphorescence: get maximums near end and other troubles
    
    %This is where phorphorescence would be processed.
    
    
end   %Phosphorescence conditional end




FWHM_XYs= [FWHM_Xs,FWHM_Ys];
Max_XYs= [Max_Xs,Max_Ys];
SS_XYs= [SS_Xs,SS_Ys];

%ACS-Nano Grey-group convention for quenching depth
%  (big number, more quenching)
QDepths =  Max_Ys./SS_Ys;

end     %Function end

