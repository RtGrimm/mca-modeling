function ...
    [GrndPopMat,SngtPopMat,TrptPopMat,...
    timeBasis,PhotonMat,TripletNet,SingletNet,GroundNet,StateChanges,TimestepMat] =...
    MCa_Feb1(randoms,...
    Nchromophores,KexcitationList,MCA_experimentConfig,inputRates,debugFeedOption)

%See accompanying testscript for example usage and plotting.

%Code was built upon the Berkeley Chemistry 'kineticMC' matlab script I
%found on the web by searching for 'kinetic monte carlo matlab'.

%I have greatly optimized it though, and many other bespoke improvements. They seemed to have left out the time
%calculation, so that is done in the manner of page 384 from Schrier's
%Intr. to Comp. Phys. Chem. (also optimized to not call RNG a billion times
%anymore).

%I recycle the random numbers and notify the user in the console output that this has happened.

% -Dave Walwark 1/2019
clc

%Assign variables from input vector
% MCA_experimentConfig= [pulseTime maxTime MCAbins MCAcycles];
pulseTime   =MCA_experimentConfig(1);
maxTime     =MCA_experimentConfig(2);
MCAbins     =MCA_experimentConfig(3);
MCAcycles   =MCA_experimentConfig(4);

exciteLevels = length(KexcitationList);


%Discretization timebasis
discreteTimeEdges=linspace(0,maxTime,MCAbins+1);

%'Soak-up' time samples that happen outside of bounds (make last bin huge)
discreteTimeEdges(MCAbins+1)=discreteTimeEdges(MCAbins+1)+1;

%What will be used for plotting
timeBasis=(linspace(0,maxTime,MCAbins))';


%Init. outputs matrices (time in rows, different excitations in columns)
PhotonMat=zeros(MCAbins,exciteLevels);
TripletNet=PhotonMat;
SingletNet=PhotonMat;
GroundNet=PhotonMat;
GrndPopMat=PhotonMat;
SngtPopMat=PhotonMat;
TrptPopMat=PhotonMat;
TimestepMat=PhotonMat;


%Size of quantization buffer
hopeLessThan=2e6;

%random number use and re-use bookkeeping
flipbit=0;
rngTicker=1;

for excSwpIndex=exciteLevels:-1:1   %Sweep over different excitation intensities
    
    % Set simulation parameters
    % ********************************************
    
    %Excitation (currently not swept over)
    Kexc = KexcitationList(excSwpIndex);
    %Fluorescence
    Kfl = inputRates(1);
    %ISC - triplet get
    Kisc = inputRates(2);
    %RISC - triplet decay
    Krisc =inputRates(3);
    %SS - singlet-singlet interaction
    Kss = inputRates(4);
    %ST - singlet-triplet interaction
    Kst = inputRates(5);
    %TT - triplet-triplet interaction
    Ktt = inputRates(6);
    %Ph - phosphorescence
    Kph = inputRates(7);
    %SF - singlet fission
    Ksf = inputRates(8);
    
    %********************************************************************
    
    %nSteps can be set high without worry, every loop iteration checks
    %to see if maxtime has been exceeded, and only steps again if it
    %has not. Probably should be while loop
    nSteps = 100000;
    %Initialize a flag
    doneFlag=0;
    
    %initialize quantization storage
    Gstacker=zeros(MCAbins,1);
    Sstacker=Gstacker;
    Tstacker=Gstacker;
    timestepstacker=Gstacker;
    
    %Initialize things to keep track of total samples in each time bin
    % (for averaging pops, or just counting state changes and photons)
    counts=Gstacker;
    TGCounts=Gstacker;
    TLCounts=Gstacker;
    SGCounts=Gstacker;
    SLCounts=Gstacker;
    GGCounts=Gstacker;
    GLCounts=Gstacker;
    PhotCounts=Gstacker;
    
    %Initializations: tickers and vars for buffers within mca loop
    photonticker=1;
    ticker=1;
    tripletLossTicker=1;
    tripletGainTicker=1;
    singletGainTicker=1;
    singletLossTicker=1;
    groundGainTicker=1;
    groundLossTicker=1;
    
    timeVec=nan(hopeLessThan,1);
    groundPop=timeVec;
    singletPop=timeVec;
    tripletPop=timeVec;
    tripletGainTimes=timeVec;
    tripletLossTimes=timeVec;
    singletGainTimes=timeVec;
    singletLossTimes=timeVec;
    groundGainTimes=timeVec;
    groundLossTimes=timeVec;
    photonEmit=timeVec;
    timesteps=timeVec;
    
    %   $$\      $$\  $$$$$$\   $$$$$$\
    %   $$$\    $$$ |$$  __$$\ $$  __$$\
    %   $$$$\  $$$$ |$$ /  \__|$$ /  $$ |
    %   $$\$$\$$ $$ |$$ |      $$$$$$$$ |
    %   $$ \$$$  $$ |$$ |      $$  __$$ |
    %   $$ |\$  /$$ |$$ |  $$\ $$ |  $$ |
    %   $$ | \_/ $$ |\$$$$$$  |$$ |  $$ |
    %   \__|     \__| \______/ \__|  \__|
    
    
    for MCArep=1:MCAcycles   %*****************************************
        %Each MCA repetition starts at time=0.
        currentTime=0;
        
%         %initialize flag for pulse edge handling
%             dontRecordFlag =0;
        
        % Set initial conditions - defining how many of each species to
        % start with
        S0 = Nchromophores;   %S0 population (ground)
        S1 = 0;             %S1 population (singlet)
        T1 = 0;             %T1 population (triplet)
        
        %Record initial conditions
        groundPop(ticker) = S0;
        singletPop(ticker) = S1;
        tripletPop(ticker) = T1;
        timeVec(ticker) = currentTime;
        ticker=ticker+1;
        
        %This reuses the random numbers when they run out
        % This is a good place to check if you get errors about indices
        % going over a certain limit.
        %This first line checks that there is enough random numbers in the
        %random number list to get through an iteration, but it is only an
        %estimation and sometimes falls short.
        if rngTicker > length(randoms) - 20/maxTime && rngTicker > 0.5*length(randoms)
            
            %Flip randoms to shake up
            randoms=flip(randoms);
            rngTicker = 1;
            if flipbit == 1
                rngTicker = 2;
            end
            flipbit=flipbit+1;
            
            if flipbit==2 %Having already flipped it, make new
                rngCap=5e6;
                %for choosing which rxn
                randoms=rand(rngCap,2);
                %Necessary logarithm for time advance
                randoms(:,2)=log(randoms(:,2));
                %reset rng-reuse ticker
                flipbit=0;
                rngTicker=1;
            end
            fprintf('Randoms list flip-reset or respawn       ')
        end %if (ticker < hopeLessThan-10000) conditional end
        
        
        %       _  __           __  __             _____
        %      | |/ /          |  \/  |           / ____|
        %      | ' /   ______  | \  / |  ______  | |
        %      |  <   |______| | |\/| | |______| | |
        %      | . \           | |  | |          | |____
        %      |_|\_\          |_|  |_|           \_____|
        % Kinetic Monte Carlo all the way across the MCA window
        for step = 1:nSteps
            
            
            if currentTime < maxTime
                
                %First-order terms ----------------------------
                
                %Turn off pulse after pulseTime over
                if currentTime < pulseTime %Pulse on
                    rate1 = Kexc*S0;
                else %Pulse is over
                    rate1 = 0;
                end
                
                rate2 = Kfl*S1 + rate1;  
                rate3 = Kisc*S1 + rate2;
                rate4 = Krisc*T1 + rate3 ;
  
                
                %Second order terms --------------------
                %Tachiya-style stoichiometry
                rate5 = Kss*0.5*S1*(S1-1) + rate4;
                
                %Singlet-triplet interaction 'checks itself' since it
                %requires 1 of each species, therefore can't progress
                %with just one ingredient
                rate6 = Kst*S1*T1 + rate5;
                
                %Tachiya-style stoichiometry
                rate7 =Ktt*0.5*T1*(T1-1) + rate6;
                
                %Phosphorescence first order
                rate8 =Kph*T1 + rate7;
                
                %Singlet-fission, god help us
                if S0 > 0  %Check for auxiliary ground state
                rate9 = Ksf*S1 + rate8;
                else
                    rate9 = 0 + rate8; %Cannot split with single chromophore
                end
                
                  
                cumulRates=[rate1, rate2, rate3,...
                    rate4, rate5, rate6,...
                    rate7, rate8, rate9];
                
                
                %Total size of 'dartboard' we throw dart at to pick
                %process
                totSum = cumulRates(end);
                
                %If nothing can happen then restart excitation loop (MCA
                %rep) REACTION-OVER CONDITION
                if totSum == 0
                    
                    %Set 'done' condition flag
                    doneFlag=1;
                    %End-time sample
                    groundPop(ticker)  = S0;
                    singletPop(ticker) = S1;
                    tripletPop(ticker) = T1; 
                    timesteps(ticker-1) = advancer;
                    timeVec(ticker) = currentTime;
                    
                    %Advance ticker
                    ticker=ticker+1;
                    
                    break  %Nothing left to do, so start MCA over!
                end
                
                % Choose a random value in the range of totSum (throw
                % dart to choose exit from current state)
                randVal = randoms(rngTicker,1)*totSum;
                
                % Get the index in the cumulRates array that this
                % random value is pointing at. The function 'find' will
                % get the index of the first species that has a
                % cumulative rate larger than the random value. It is the
                % computationally limiting step.
                index = find(cumulRates > randVal,1);
                
                %--------------------------------------------------------
                %Keep track of time here, ala wikipedia on KMC There is
                %a logarithm performed 'here' on the random value, its
                %just done at the beginning of the script en masse.
                %Random time advancement:
                advancer=(-1/(totSum))*randoms(rngTicker,2);
                %tenatative time advancement (The 'future' at the moment)
                currentTimeTmp = currentTime + advancer;
                
                %--------------------------------------------------------------------
                
                %Check whether straddling the pulse edge, set flag
                if currentTimeTmp > pulseTime && currentTime < pulseTime
                    straddle=1;
                else
                    straddle=0;
                end
                
                %If asked for, display which reaction happened
                if debugFeedOption ==1
                    disp(['Reaction ' num2str(index) ' chosen'])
                end
                
                
                % Apply the correct reaction based on the index chosen
                
                if index == 1  %Ground -> singlet (excitation)
                    
                    if straddle==0 % If legit, do something
                        S0 = S0 - 1;
                        S1 = S1 + 1;
                        %Mark time and advance indices
                        singletGainTimes(singletGainTicker)= currentTimeTmp;
                        singletGainTicker=singletGainTicker+1;
                        groundLossTimes(groundLossTicker)= currentTimeTmp;
                        groundLossTicker=groundLossTicker+1;
                    
                    else %The Keller Correction - no absorption, simulation moved to pulse edge time
                        currentTimeTmp = pulseTime;
                        advancer = pulseTime - currentTime;
%                         dontRecordFlag = 1;
                        %S0 and S1 remain the same - no unphysical
                        %absorption         
                    end
                    
                elseif index == 2  %Singlet -> ground (photon emitted)
                    S0 = S0 + 1;
                    S1 = S1 - 1;
                    %Mark time and advance indices
                    groundGainTimes(groundGainTicker)= currentTimeTmp;
                    groundGainTicker=groundGainTicker+1;
                    singletLossTimes(singletLossTicker)= currentTimeTmp;
                    singletLossTicker=singletLossTicker+1;
                    %Mark emission time and advance photon index
                    photonEmit(photonticker) = currentTimeTmp;
                    photonticker = photonticker + 1;          
                    
                elseif index == 3 %singlet -> triplet
                    T1 = T1 + 1;
                    S1 = S1 - 1;
                    %Mark time and advance indices
                    tripletGainTimes(tripletGainTicker)= currentTimeTmp;
                    tripletGainTicker=tripletGainTicker+1;
                    singletLossTimes(singletLossTicker)= currentTimeTmp;
                    singletLossTicker=singletLossTicker+1;
                    
                elseif index == 4 %triplet -> ground
                    T1 = T1 - 1;
                    S0 = S0 + 1;
                    %Mark time and advance indices
                    tripletLossTimes(tripletLossTicker)= currentTimeTmp;
                    tripletLossTicker=tripletLossTicker+1;
                    groundGainTimes(groundGainTicker)= currentTimeTmp;
                    groundGainTicker=groundGainTicker+1;
                    
                elseif index == 5 %singlet-singlet annihilation
                    S1 = S1 - 2;
                    S0 = S0 + 2;
                    %Mark time and advance indices
                    singletLossTimes(singletLossTicker)= currentTimeTmp;
                    singletLossTicker=singletLossTicker+1;
                    singletLossTimes(singletLossTicker)= currentTimeTmp;
                    singletLossTicker=singletLossTicker+1;
                    groundGainTimes(groundGainTicker)= currentTimeTmp;
                    groundGainTicker=groundGainTicker+1;
                    groundGainTimes(groundGainTicker)= currentTimeTmp;
                    groundGainTicker=groundGainTicker+1;
                    
                elseif index == 6 %singlet-triplet annihilation
                    S1 = S1 - 1;
                    T1 = T1 - 0;
                    S0 = S0 + 1;
                    
                    %%This can depend on mechanism like Forster or Dexter
                    
                    %Mark time and advance indices
                    singletLossTimes(singletLossTicker)= currentTimeTmp;
                    singletLossTicker=singletLossTicker+1;
%                     tripletLossTimes(tripletLossTicker)= currentTimeTmp;
%                     tripletLossTicker=tripletLossTicker+1;
                    groundGainTimes(groundGainTicker)= currentTimeTmp;
                    groundGainTicker=groundGainTicker+1;
%                     groundGainTimes(groundGainTicker)= currentTimeTmp;
%                     groundGainTicker=groundGainTicker+1;
                    
                elseif index == 7 %triplet-triplet fusion
                    T1 = T1 - 2;
                    S0 = S0 + 1;
                    S1 = S1 + 1;
                    %Mark time & advance tickers (lose two triplets)
                    tripletLossTimes(tripletLossTicker)= currentTimeTmp;
                    tripletLossTicker=tripletLossTicker+1;
                    tripletLossTimes(tripletLossTicker)= currentTimeTmp;
                    tripletLossTicker=tripletLossTicker+1;
                    %Get a singlet out of the deal
                    singletGainTimes(singletGainTicker)= currentTimeTmp;
                    singletGainTicker=singletGainTicker+1;
                    %As well as a ground state
                    groundGainTimes(groundGainTicker)= currentTimeTmp;
                    groundGainTicker=groundGainTicker+1;
                              
                    
                    elseif index ==8 %triplet -> ground + PHOTON
                    T1 = T1 - 1;
                    S0 = S0 + 1;
                    %Mark time and advance indices
                    tripletLossTimes(tripletLossTicker)= currentTimeTmp;
                    tripletLossTicker=tripletLossTicker+1;
                    groundGainTimes(groundGainTicker)= currentTimeTmp;
                    groundGainTicker=groundGainTicker+1;
                    photonEmit(photonticker) = currentTimeTmp;
                    photonticker = photonticker + 1;
                    
                elseif index == 9  %Singlet-fission
                    T1 = T1 + 2; %Get two triplets
                    S1 = S1 - 1; %Lose one singlet
                    S0 = S0 - 1; %Lose one ground (must conserve mass!)
                    tripletGainTimes(tripletGainTicker)= currentTimeTmp;
                    tripletGainTicker=tripletGainTicker+1;
                    tripletGainTimes(tripletGainTicker)= currentTimeTmp;
                    tripletGainTicker=tripletGainTicker+1;
                    singletLossTimes(singletLossTicker)= currentTimeTmp;
                    singletLossTicker=singletLossTicker+1;
                    groundLossTimes(groundLossTicker)= currentTimeTmp;
                    groundLossTicker=groundLossTicker+1;
                    
                end  % End/Finish reaction consequences
                
                %Resolve time increase because it's kosher
                currentTime=currentTimeTmp;               
                
%                 if dontRecordFlag ==1
%                      %%Record consequences of reaction
%                 groundPop(ticker)  = nan;
%                 singletPop(ticker) = nan;
%                 tripletPop(ticker) = nan;
%                 timeVec(ticker) = currentTime;
%                 timesteps(ticker-1) = advancer;
%                     
%                 else

%                 %%Record consequences of reaction
                groundPop(ticker)  = S0;
                singletPop(ticker) = S1;
                tripletPop(ticker) = T1;
                timeVec(ticker) = currentTime;
                timesteps(ticker-1) = advancer;
                
%                 end
                
                %Advance ticker
                ticker=ticker+1;

                if debugFeedOption ==1
                    disp(currentTime)
                end
                %having used both columns of the rng matrix, move to next
                %row
                rngTicker=rngTicker+1;
                
            else %Time's up so exit loop
                doneFlag=1;
                % exit loop
                if debugFeedOption ==1
                    TimesUpTime=currentTime
                end
                break %%conditional timelimit consequence
                
                
            end  %conditional timelimit end
            
        end %Timestep loop end
        
        
        %                    _   _
        %                   | | | |
        %      ___ _ __   __| | | | ___ __ ___   ___
        %     / _ \ '_ \ / _` | | |/ / '_ ` _ \ / __|
        %    |  __/ | | | (_| | |   <| | | | | | (__
        %     \___|_| |_|\__,_| |_|\_\_| |_| |_|\___|
        
        %Everything below here is dealing with quantization and data
        %storage/output. No consequences for reality of simulation (other
        %than if you chose a really small bitdepth at the beginning of the
        %script and now the quantization processes below are oversmoothing
        %your results)
        
        % quantize when buffer full or when simulation over
        if ticker > 0.75*hopeLessThan 
            %Quantize/discretize the time samples
                                  
            disp(['Quantizing ' num2str(photonticker) ' photons'])
            
            %Ticker offsets since they've been advanced prematurely
            ticker=ticker-1;
            photonticker=photonticker-1;
            tripletLossTicker=tripletLossTicker-1;
            tripletGainTicker=tripletGainTicker-1;
            singletGainTicker=singletGainTicker-1;
            singletLossTicker=singletLossTicker-1;
            groundGainTicker=groundGainTicker-1;
            groundLossTicker=groundLossTicker-1;
            
            % Take all time samples and quantize them
            TimeBinAssignments = ...
                discretize(timeVec(1:ticker),discreteTimeEdges);
            PhotonTimeBinAssignments = ...
                discretize(photonEmit(1:photonticker),discreteTimeEdges);
            
            GroundGainTimeBinAssignments = ...
                discretize(groundGainTimes(1:groundGainTicker),discreteTimeEdges);
            GroundLossTimeBinAssignments = ...
                discretize(groundLossTimes(1:groundLossTicker),discreteTimeEdges);
            
            SingletGainTimeBinAssignments = ...
                discretize(singletGainTimes(1:singletGainTicker),discreteTimeEdges);
            SingletLossTimeBinAssignments = ...
                discretize(singletLossTimes(1:singletLossTicker),discreteTimeEdges);
            
            TripletGainTimeBinAssignments = ...
                discretize(tripletGainTimes(1:tripletGainTicker),discreteTimeEdges);
            TripletLossTimeBinAssignments = ...
                discretize(tripletLossTimes(1:tripletLossTicker),discreteTimeEdges);
            
            %Use those discretization assignments to build a new variable
            %that sums samples together based on where they appear in time
            %Each ticker needs its own for-loop
            for ind = 1:ticker
                %                 try
                Gstacker(TimeBinAssignments(ind)) ...
                    = groundPop(ind) + Gstacker(TimeBinAssignments(ind));
                Sstacker(TimeBinAssignments(ind))...
                    = singletPop(ind) + Sstacker(TimeBinAssignments(ind));
                Tstacker(TimeBinAssignments(ind))...
                    = tripletPop(ind) + Tstacker(TimeBinAssignments(ind));
                timestepstacker(TimeBinAssignments(ind))...
                    = timesteps(ind) + timestepstacker(TimeBinAssignments(ind));
                %                 catch
                %                     ticker
                %                 end
                
            end  %Necessary assignment for-loop:  'for ind = 1:useTicker'
                  
            %Keep track of number lumped together for later reckoning
            %For discrete counts, this number IS the data, not just an
            %averaging factor, as it is for the direct population estimates.
            
            %Samples in each time bin 
            counts = counts + (histcounts(TimeBinAssignments,0.5:1:MCAbins+1))';
            %Photons in each time bin
            PhotCounts = PhotCounts + (histcounts(PhotonTimeBinAssignments,0.5:1:MCAbins+1))';
            
            %State changes in each time bin (
            GGCounts = GGCounts + (histcounts(GroundGainTimeBinAssignments,0.5:1:MCAbins+1))';
            GLCounts = GLCounts + (histcounts(GroundLossTimeBinAssignments,0.5:1:MCAbins+1))';
            
            SGCounts = SGCounts + (histcounts(SingletGainTimeBinAssignments,0.5:1:MCAbins+1))';
            SLCounts = SLCounts + (histcounts(SingletLossTimeBinAssignments,0.5:1:MCAbins+1))';
            
            TGCounts = TGCounts + (histcounts(TripletGainTimeBinAssignments,0.5:1:MCAbins+1))';
            TLCounts = TLCounts + (histcounts(TripletLossTimeBinAssignments,0.5:1:MCAbins+1))';
            
            %             if doneFlag==0
            %Finally, reset tickers for buffer re-use
            photonticker=1;
            ticker=1;
            tripletLossTicker=1;
            tripletGainTicker=1;
            singletGainTicker=1;
            singletLossTicker=1;
            groundGainTicker=1;
            groundLossTicker=1;
           
        end   %ticker-high OR done-donemax time for discretization condition
        
    end %MCA rep loop end
    
    
    
    %Quantize/discretize the time samples
    
    if (doneFlag == 1  &&  MCArep == MCAcycles )
        disp(' MCA sweeps complete ')
    end
    
    
    disp(['Quantizing ' num2str(photonticker) ' photons' ' - done'])
    
    %Ticker offsets since they've been advanced prematurely
    ticker=ticker-1;
    photonticker=photonticker-1;
    tripletLossTicker=tripletLossTicker-1;
    tripletGainTicker=tripletGainTicker-1;
    singletGainTicker=singletGainTicker-1;
    singletLossTicker=singletLossTicker-1;
    groundGainTicker=groundGainTicker-1;
    groundLossTicker=groundLossTicker-1;
    
    % Take all time samples and quantize them
    TimeBinAssignments = ...
        discretize(timeVec(1:ticker),discreteTimeEdges);
    PhotonTimeBinAssignments = ...
        discretize(photonEmit(1:photonticker),discreteTimeEdges);
    
    GroundGainTimeBinAssignments = ...
        discretize(groundGainTimes(1:groundGainTicker),discreteTimeEdges);
    GroundLossTimeBinAssignments = ...
        discretize(groundLossTimes(1:groundLossTicker),discreteTimeEdges);
    
    SingletGainTimeBinAssignments = ...
        discretize(singletGainTimes(1:singletGainTicker),discreteTimeEdges);
    SingletLossTimeBinAssignments = ...
        discretize(singletLossTimes(1:singletLossTicker),discreteTimeEdges);
    
    TripletGainTimeBinAssignments = ...
        discretize(tripletGainTimes(1:tripletGainTicker),discreteTimeEdges);
    TripletLossTimeBinAssignments = ...
        discretize(tripletLossTimes(1:tripletLossTicker),discreteTimeEdges);
    
    %Use those discretization assignments to build a new variable
    %that sums samples together based on where they appear in time
    %Each ticker needs its own for-loop
    for ind = 1:ticker
%                         try
        Gstacker(TimeBinAssignments(ind)) ...
            = groundPop(ind) + Gstacker(TimeBinAssignments(ind));
        Sstacker(TimeBinAssignments(ind))...
            = singletPop(ind) + Sstacker(TimeBinAssignments(ind));
        Tstacker(TimeBinAssignments(ind))...
            = tripletPop(ind) + Tstacker(TimeBinAssignments(ind));
        timestepstacker(TimeBinAssignments(ind))...
            = timesteps(ind) + timestepstacker(TimeBinAssignments(ind));
%         
%         
%                         catch
%                             ticker
%                         end
        
    end  %Necessary assignment for-loop:  'for ind = 1:useTicker'
    
    
    %Keep track of number lumped together for later reckoning
    %For discrete counts, this number IS the data, not just an
    %averaging factor, as it is for the direct population estimates.
    counts = counts + (histcounts(TimeBinAssignments,0.5:1:MCAbins+1))';
    PhotCounts = PhotCounts + (histcounts(PhotonTimeBinAssignments,0.5:1:MCAbins+1))';
    
    GGCounts = GGCounts + (histcounts(GroundGainTimeBinAssignments,0.5:1:MCAbins+1))';
    GLCounts = GLCounts + (histcounts(GroundLossTimeBinAssignments,0.5:1:MCAbins+1))';
    
    SGCounts = SGCounts + (histcounts(SingletGainTimeBinAssignments,0.5:1:MCAbins+1))';
    SLCounts = SLCounts + (histcounts(SingletLossTimeBinAssignments,0.5:1:MCAbins+1))';
    
    TGCounts = TGCounts + (histcounts(TripletGainTimeBinAssignments,0.5:1:MCAbins+1))';
    TLCounts = TLCounts + (histcounts(TripletLossTimeBinAssignments,0.5:1:MCAbins+1))';
    
    
    
    %Final stuff ++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    %If no count received, make sure not to divide by zero.
    %We can't initialize with ones because they get added upon
    counts(counts==0) = 1;
    
    
    %Now get average population for time bin
    Gstacker=Gstacker./counts;
    Sstacker=Sstacker./counts;
    Tstacker=Tstacker./counts;
    
    %And the timesteps
    timestepstacker=timestepstacker./counts;
      
    %Assign final outputs
    PhotonMat(:,excSwpIndex) =PhotCounts;
    TripletNet(:,excSwpIndex) = TGCounts - TLCounts;
    SingletNet(:,excSwpIndex) = SGCounts - SLCounts;
    GroundNet(:,excSwpIndex) =  GGCounts - GLCounts;
    StateChanges(:,excSwpIndex) = TGCounts+TLCounts+SGCounts+SLCounts+GGCounts+GLCounts;
    GrndPopMat(:,excSwpIndex) = Gstacker;
    SngtPopMat(:,excSwpIndex) = Sstacker;
    TrptPopMat(:,excSwpIndex) = Tstacker;
    TimestepMat(:,excSwpIndex) = timestepstacker;
   
    
end %Excitation sweep loop end

end  %FUNCTION END