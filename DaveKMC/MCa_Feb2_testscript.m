%Run-script for MCA_MC function -D. J. Walwark 2019

%Molecular Configuration
%=====================================================
% Estimated quantum yield:
Phi_fl = 0.5;

%Number of chomophores (MCAcycles is divided by sqrt(this number) to control
%simulation time)
Nchromophores=4;

%Kfl - Emission - singlet -> ground
Kfl = 2e9;

%What fraction of the fluorescence rate should ISC be?
%Input hard-code rate here for intersystem crossing
KiscP3HT = 1e9;
iscMult=KiscP3HT/Kfl;      % iscMult=0;

%What fraction of the fluorescence  rate should REVERSE-ISC be?
% riscMult=0.000156; %(64 microsecond lifetime)
%Input hard-code rate here for intersystem crossing
KriscP3HT = 1e4;
riscMult=KriscP3HT/Kfl;    % riscMult=0;

%_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*

%Set 'higherOrderScaling' to zero if no biexciton interactions are
%desired.
higherOrderScaling=0;

%What fraction of the fluorescence rate should singlet-singlet be?
%Singlet-singlet interactions are a problem for high-power pulsed
%experiments but not much of a concern for us. 
ssMult = 0;

%What fraction of the fluorescence rate should singlet-triplet be?
stMult = 0.01; stMult = 1;

%What fraction of the fluorescence rate should triplet-triplet be?
 ttMult=0.000001; ttMult=0.0;

%What fraction of the fluorescence rate should phosphorescence be?
phMult=0;

%fraction of Kfl what singlet fission is
sfMult=0;

 

%MCA Experimental Configuration
%=====================================================
% How many MCA cycles to perform
% This is the most important parameter for determining simulation runtime.
MCAcycles = 10000 ;

%Instrument limit is 100ns bins. What size bins do you want?
desiredBinSize=500e-9;

%How long is pulse on during min excitation? (the longest pulse time)
pulseTimeAtExcMin = 1000e-6;

%Time each stairstep takes up (including pulse-off time)
maxTime = 1.01*pulseTimeAtExcMin;

% Estimated or desired maximum photons per second (excitation rate)
maxKexc = 5e5;  %Used to scale the linspace, not a guarantee

% Minimum excitation photons per second in sweep 
minKexc = 1000;

% Number of excitation steps in MCA trace (4 is fastest for most computers)
excSteps = 12;      

% % Ratio of adjacent steps (old Grey-group data isn't this simple, since it
% % is usually 0.25, 0.5, 0.75, 1, which isn't a constant ratio )
stepRatio = 2;

%How to scale the time as a function of power
% Closer to 1/2 or 1 makes high-excitation pulse-time very short compared to
% low-excitation pulse-time. 
% Closer to ~0 makes high-excitation-pulse-time same as low excitation
timeScaleFrac = 1*(1/(stepRatio*2));

%------------------------------
%Configure stairstep heights and pulse time stretching explicitly here

%Ad-hoc parameter space scaling

%Use 3/4 for getting to the knee in linspace.
propParamsLinear = 3/4;

%A linear space for lower intensity excitation
KexcStartList=(linspace(minKexc,maxKexc/12,(propParamsLinear)*excSteps));
% - complemented with a geometric space for the higher intensities
for n = 1:100   %More than you'll ever need
KexcGeometricEndList(n) = (stepRatio^n)*KexcStartList(end);
end
%
%Whatever room is left over in the xcitation list, fill it with
%geometrically increasing numbers determined by the stepRatio and the total
%desired excitation steps. Maximum excitation is not well defined
discrepancyTmp= excSteps - length(KexcStartList);
KexcList = [KexcStartList   KexcGeometricEndList(1:discrepancyTmp)];

%Apply quantum efficiency scaling
KexcList=KexcList*Phi_fl;

% KexcList = Phi_fl*...
%     [(linspace(minKexc,maxKexc/10,(3/4)*excSteps)) (linspace(maxKexc/10,maxKexc,(1/4)*excSteps))];
% excStepsVec = excSteps:-1:1;
% KexcRatioList = [];   pulseTimeRatioList= [];
% 
% for n=1:excSteps
% KexcRatioList(n) = (1/stepRatio)^(excStepsVec(n)-1);   %#ok<SAGROW>
% pulseTimeRatioList(n) = ((stepRatio)^(timeScaleFrac))^(excStepsVec(n)-1);   %#ok<SAGROW>
% end
% KexcList = Phi_fl*maxKexc*KexcRatioList;  %List of simulated absorption rates
% pulseTimeRatioList=pulseTimeRatioList./max(pulseTimeRatioList);
%-------------------------------------------------

% Set up the perfect experiment, with more pulse time for lower excitations
% pulseTimeVec = [];
% pulseTimeVec = pulseTimeRatioList*pulseTimeAtExcMin;
pulseTimeVec =flip(linspace(pulseTimeAtExcMin/4,pulseTimeAtExcMin,excSteps));

%This many time-bins total
numberBins = floor(maxTime/(desiredBinSize));

% MCA bins to use across 'maxTime'. Use 'numberBins' or circumvent as needed.
MCAbins = numberBins;

%Normalize by chromophores to make run-times more consistent
MCAcycles=ceil(MCAcycles/sqrt(Nchromophores));
if MCAcycles <1
    MCAcycles =1;
end

MCA_experimentConfig = zeros(excSteps,4);

try
MCA_experimentConfig(1:excSteps,1) = pulseTimeVec';
MCA_experimentConfig(:,2) = maxTime;
MCA_experimentConfig(:,3) = MCAbins;
MCA_experimentConfig(:,4) = MCAcycles;
catch
    clear MCA_experimentConfig
    MCA_experimentConfig(1:excSteps,1) = pulseTimeVec';
MCA_experimentConfig(:,2) = maxTime;
MCA_experimentConfig(:,3) = MCAbins;
MCA_experimentConfig(:,4) = MCAcycles;
end

%-------------------------------------------------------

%Define rates and use as function input. 

%Kisc - InterSystem Crossing - singlet -> triplet
Kisc = Kfl*iscMult;

%Kisc - InterSystem Crossing - singlet -> triplet
Krisc = Kfl*riscMult;

%Kss - %singlet-singlet annihilation
Kss = Kfl*ssMult*higherOrderScaling;

%Kst - singlet-triplet annihilation
Kst = Kfl*stMult*higherOrderScaling;

%Ktt - triplet-triplet annihilation
Ktt = Kfl*ttMult*higherOrderScaling;

%Kph - phosphorescence
Kph = Kfl*phMult;

%Singlet fission
Ksf = Kfl*sfMult;

%Load em up
Rates = [Kfl, Kisc, Krisc, Kss, Kst, Ktt, Kph, Ksf];

%For labeling
RatesToSee=round(Rates,3,'significant')/1e6;

%Annotation
RateListStringCell = {['Kfl = ' num2str( RatesToSee(1)) ' MHz'],...
    ['Kisc = '   num2str(iscMult) '*' 'Kfl ' ' (' num2str(RatesToSee(2)) ' MHz)' ],...
    ['Krisc = '   num2str(riscMult) '*' 'Kfl ' ' (' num2str(RatesToSee(3)) ' MHz)' ],...
    ['Kss = '   num2str(ssMult) '*' 'Kfl ' ' (' num2str(RatesToSee(4)) ' MHz)' ],...
    ['Kst = '   num2str(stMult) '*' 'Kfl ' ' (' num2str(RatesToSee(5)) ' MHz)' ],...
    ['Ktt = '   num2str(ttMult) '*' 'Kfl ' ' (' num2str(RatesToSee(6)) ' MHz)' ],...
    ['Kph = '   num2str(phMult) '*' 'Kfl ' ' (' num2str(RatesToSee(7)) ' MHz)' ],...
    ['Ksf = '   num2str(sfMult) '*' 'Kfl ' ' (' num2str(RatesToSee(8)) ' MHz)' ]};


%% Execution of Monte Carlo with above settings

%Random number generation and reusable stockpile
% It is nice for debugging to have a random number list that doesn't change
% until you want it to. Use the 'if' statement to hold list constant.
rngCap=5e6;  %Length of random number list. It has 2 columns.
if exist('randoms', 'var') == 0
    %for choosing which rxn
    randoms=rand(rngCap,2);
    %Necessary logarithm for time advance
    randoms(:,2)=log(randoms(:,2));
end

%If you set MCAcycles to 1 you automatically get debug mode
if MCAcycles ==1
    debugYesOrNoBinary = 1;
else
    debugYesOrNoBinary = 0;
end

%Initialize output of simulation matrices
GPop = zeros(MCAbins,excSteps);
SPop = GPop;
TPop = GPop;
PhotonCts = GPop;
TripletNet = GPop;
SingletNet = GPop;
GroundNet = GPop;
Timesteps = GPop;
StateChanges= GPop;

% profile on

parfor Ind = 1:excSteps  %Computes different chromophore number sims
    
    %     %The monte carlo simulation
    [GPop(:,Ind),SPop(:,Ind),TPop(:,Ind),timeBasis,PhotonCts(:,Ind),...
        TripletNet(:,Ind),SingletNet(:,Ind),GroundNet(:,Ind),StateChanges(:,Ind),Timesteps(:,Ind)] =...
        MCa_Feb1(...
        randoms,Nchromophores,...
        KexcList(Ind),MCA_experimentConfig(Ind,:),...
        Rates,debugYesOrNoBinary);
    
end %parfor end


%What will be used for plotting
timeBasis=(linspace(0,maxTime,MCAbins))';

% profile off
% profile report


%% Post-processing and plotting




clc


%
[QDepths,Max_XYs,SS_XYs,FWHM_XYs,smoothedCts] = ...
    MCA_ClassPtCollector(...
    timeBasis,pulseTimeVec,maxTime,PhotonCts,0);


%%

figure('Position',[200 200 600 500]);
% subplot(1,2,1)
% plot(KexcList,1./FWHM_XYs(:,1))
% xlabel('Excitation')
% ylabel('Half-quench rate')
% subplot(1,2,2)
plot(KexcList,QDepths)
xlabel('Excitation')
ylabel('Quenching Depths: Imax/Iss')

figure('Position',[200 200 600 500]);
% subplot(1,2,1)
hold on
plot(KexcList,SS_XYs(:,2))
% xlabel('Excitation')
% ylabel('I_{ss}')

% subplot(1,2,2)
plot(KexcList,Max_XYs(:,2))
xlabel('Excitation')
% ylabel('I_{max}')
plot(KexcList,1./FWHM_XYs(:,1))

legend('Steady State Intensity',...
        'Maximum Intensity','Half-quench rate',...
        'Box','off','Location','East')


%%

figure;

for n = 1:excSteps
hold on
                

                plot(timeBasis,PhotonCts(:,n));
                plot(timeBasis,smoothedCts(:,n),'LineWidth',1.5);
                scatter(Max_XYs(n,1),Max_XYs(n,2),50,'b','Filled');
%                 yline(HalfMaxes(n));
                scatter(FWHM_XYs(n,1),FWHM_XYs(n,2),50,[0 0.7 0],'Filled');
%                 xline(FWHM_Time);
                %scatter to start of steady state window and calcd SS value
                scatter(SS_XYs(n,1),SS_XYs(n,2),50,'k','LineWidth',1);

end 

% xlim([-1e-7 max(timeBasis)/1.95])
%%


%what (beginning) portion of pulse to exclude from steady state estimate
excludeProportion=0.7;

%This function finds median population in user-defined steady state of sampled
%population traces WITHOUT REACHING A STEADY-STATE IT WILL BE WRONG
[steadyGround, steadySinglet, steadyTriplet] = ...
    DJWSS(GPop,SPop,TPop,...
    maxTime,pulseTimeVec,excludeProportion,Nchromophores);

%For ground state, get span
groundSpan = Nchromophores - steadyGround;


%Integrate the explicit state changes (make non-normalized pdfs)
TripletPDFcurve=cumsum(TripletNet);
SingletPDFcurve=cumsum(SingletNet);
GroundPDFcurve=cumsum(GroundNet);


%Estimate steady state for integrated derivatives (SS of non-normalized pdfs)
[steadyGroundIntegrated, steadySingletIntegrated, steadyTripletIntegrated] = ...
    DJWSS(abs(GroundPDFcurve),SingletPDFcurve,TripletPDFcurve,...
    maxTime,pulseTimeVec,excludeProportion,Nchromophores);

GroundProbEst = GroundPDFcurve./steadyGroundIntegrated;
GroundProbEst = (GroundProbEst .* groundSpan) + Nchromophores;

%Dubious correction to keep from dividing by zero in next lines
steadySingletIntegrated(steadySingletIntegrated==0)=1;
steadyTripletIntegrated(steadyTripletIntegrated==0)=1;

%Normalize integrated triplet differentials
TripletProbEst=steadyTriplet.*TripletPDFcurve./steadyTripletIntegrated;
SingletProbEst=steadySinglet.*SingletPDFcurve./steadySingletIntegrated;

%Put all the excitation level plots together
OneVecTripletProbEst =TripletProbEst(:);
OneVecSingletProbEst =SingletProbEst(:);
OneVecGroundProbEst =GroundProbEst(:);

%Make encompassing timebasis
OneVecTimeBasis=[];
for n =1:excSteps
    OneVecTimeBasis =vertcat(OneVecTimeBasis,(timeBasis +(n-1)*maxTime)); %#ok<AGROW>
    %     [timeBasis',timeBasis'+maxTime,...
    %     timeBasis'+2*maxTime,timeBasis'+3*maxTime,...
    %     timeBasis'+4*maxTime,timeBasis'+5*maxTime];
end

%Open figure window for linear & log non-normalized plot
MCAfigure=figure('Position',[200 50 1000 750]);

%If smoothing is needed, elevate this number. Usually I just turn the bit
%depth down in the function itself.
smthParam=1;

for exciteIndex=excSteps:-1:1
    subplot(2,1,1)
    timeBasisFlexible=timeBasis+(exciteIndex-1)*maxTime;
    
    plotPtmp=smooth(PhotonCts(:,exciteIndex),smthParam*1,'moving');
    
    hold on
    yyaxis left
    area(timeBasisFlexible,plotPtmp,...
        'EdgeAlpha',1,'FaceAlpha',0.5,'FaceColor',[0.0 0.3 0.8])
    
    try
        ylim([0 1.08*max(PhotonCts(:,end))])
    catch
        ylim([0 Inf])
    end
    
    ylabel('Counts')
    xlim([-0.1*maxTime excSteps*maxTime-0.1*maxTime])
    
    yyaxis right
    hold on
    %  sing1=plot(OneVecTimeBasis,OneVecSingletProbEst,...
    %         '-','LineWidth',2,'Color',[0.9 0.4 0 0.5]);
    %     sing1.Color(4) =0.07;
    
    trip1=plot(OneVecTimeBasis,OneVecTripletProbEst,...
        '-','LineWidth',2,'Color',[0.1 0.2 0 0.8]);
    trip1.Color(4) =0.1;
    
    
    
    ylabel('# States ')
    
    ylim([0 Nchromophores*1.1])
    
    xlabel('Time (s)')
    
    subplot(2,1,2)
    %The log y-scale subplot (bottom half)
    hold on
    yyaxis left
    area(timeBasisFlexible,plotPtmp,...
        'EdgeAlpha',1,'FaceAlpha',0.5,'FaceColor',[0.0 0.3 0.8])
    
    try
        ylim([10 1.08*max(PhotonCts(:,end))])
    catch
        ylim([0 Inf])
    end
    
    set(gca,'YScale','log')
    ylabel('Log(Counts)')
    yyaxis right
    hold on
    %     sing2=plot(OneVecTimeBasis,OneVecSingletProbEst+0.01,...
    %         '-','LineWidth',2,'Color',[0.9 0.4 0 0.5]);
    % sing2.Color(4) =0.07;
    
    trip2=plot(OneVecTimeBasis,OneVecTripletProbEst+0.01,...
        '-','LineWidth',2,'Color',[0.1 0.2 0 0.8]);
    trip2.Color(4) =0.07;
    
    set(gca,'YScale','log')
    ylim([0.01 Nchromophores*1.1])
    xlim([-0.1*maxTime excSteps*maxTime-0.1*maxTime])
end

%Put annotation in first subplot, left axis coordinates
subplot(2,1,1)
yyaxis left
%Annotation
annotPositX=0.1*maxTime;
annotPositY= 0.75*max(PhotonCts(:,end));
text(annotPositX,annotPositY,RateListStringCell,'FontSize',10)

sgtitle({[num2str(Nchromophores) ' chromophores with QY = ' num2str(Phi_fl)],'Kexc (MHz) = ', num2str(round(2*KexcList,3,'significant')/1e6)})

%%
% extremely detailed plot - all population estimates and triplet population
% via integration and scaling by steady state of population estimate

% Emission results are NORMALIZED

figure;
for exciteIndex=1:excSteps
%     subplot(2,1,1)
    
    timeBasisFlexible=timeBasis+(exciteIndex-1)*maxTime;
    
    %Smoothing, which I am currently not using
    plotGtmp=smooth( GPop(:,exciteIndex),smthParam,'moving');
    plotStmp=1*smooth( SPop(:,exciteIndex),smthParam,'moving');
    plotTtmp=smooth( TPop(:,exciteIndex),smthParam,'moving');
    
    plottimesteptmp=smooth( Timesteps(:,exciteIndex),smthParam*1,'moving');
    plotPtmp=smooth( PhotonCts(:,exciteIndex),smthParam*1,'moving');
    plotTGtmp=smooth( TripletNet(:,exciteIndex),smthParam*1,'moving');
    
    
    %adhoc de-noise for ground state - assumes ground state returns to full
    %occupancy
    
    if MCAcycles ==1
    else
    plotGtmp(plotGtmp==0)=Nchromophores;
    end
    
    PlotIntTripletChange=TripletProbEst(:,exciteIndex);
    PlotIntSingletChange=SingletProbEst(:,exciteIndex);
    PlotIntGroundChange=GroundProbEst(:,exciteIndex);
    hold on
    
    %Integrated ground-state differential (noisy during pulse, gorgeous
    %after pulse ends)
%     plot(timeBasisFlexible,PlotIntGroundChange,'Color',[0.1 0.2 0.8])
    
    %Photon emit
    photonPlot=area(timeBasisFlexible,Nchromophores*plotPtmp/max(plotPtmp),...
        'EdgeAlpha',0.3,'FaceAlpha',1);
    photonPlot.FaceColor = [0.5 0.7 1];
    
    plot(timeBasisFlexible,plotGtmp,'Color',[0 0.1 0.6])
    plot(timeBasisFlexible,plotStmp,'Color',[1 0.5 0])
    plot(timeBasisFlexible,plotTtmp,'LineStyle','--','Color',[0.2 0.5 0])
%     plot(timeBasisFlexible,PlotIntTripletChange,'Color',[0 0.6 0])
    
    %      plot(timeBasisFlexible,PlotIntSingletChange,'Color',[1 0.6 0])
    
%     legend('Ground Pop. (via integral)','Photons (normalized)',...
%         'Ground Pop.','Singlet Pop. ','Triplet Pop.','Triplet Pop. (via integral)',...
%         'Box','off','Location','East')
%     
%     
    
    legend('Photons (normalized)',...
        'Ground Pop.','Singlet Pop. ','Triplet Pop.',...
        'Box','off','Location','East')
    
    
    
    %     yyaxis right
    %     % xlim([-1e-6 maxTime])
    ylim([0 1.1*Nchromophores])
    xlim([-0.1*maxTime excSteps*maxTime+1*maxTime])

%     % ylim([-Inf 1.05])
%     subplot(2,1,2)
%     hold on
%         plot(timeBasisFlexible,plottimesteptmp)
%         ylim([0 1e-3])
%         legend('Timestep Size',...
%         'Box','off','Location','East')
    
%     set(gca,'YScale','log')
    % %     area(timeBasis,plotPtmp/max(plotPtmp),...
    % %         'EdgeAlpha',0,'FaceAlpha',0.5)
    %     % histogram(PhotonCts(:,exciteIndex),100,...
    %     %     'Normalization','probability','EdgeAlpha',0)
    %     % histogram(PhotonCts(:,exciteIndex),100,...
    %     %     'EdgeAlpha',0)
    %
    % %     xlim([-0.1e-6 Inf])
    %     ylim([-Inf 1.05])
end
% xlim([0 maxTime/1])
title({[num2str(Nchromophores) ' chromophores'],'Kexc (MHz) = ', num2str(round(KexcList,3,'significant')/1e6)})

xlim([-0.1*maxTime excSteps*maxTime+1*maxTime])

