function [steadyGround, steadySinglet, steadyTriplet] = ...
    DJWSS(GroundPop,SingletPop,TripletPop,...
    totalTime,pulseTimeVec,excludeProportion,n_Chromophores)

%DJWsteadystate - Takes population samples from monte carlo and finds
%probability at steady state. 'excludeProportion' is the proportion of the
%trace you would discard as being not-steady-state. A number like 0.4 will
%exclude the first 40% of the pulse emission from the steady state
%estimation. D. Walwark - 1-22-19

pulseProportionVec = pulseTimeVec./totalTime;

numSamples=length(TripletPop);

% Index where pulse ends
pulseEndIndex = floor(pulseProportionVec*numSamples);
pulseEndIndex = pulseEndIndex - 2;  %Arbitrary exclusion of a few samples

% Index where steady state starts
ssStartIndex = ceil(excludeProportion*pulseProportionVec*numSamples);

for ind= 1:size(TripletPop,2)
%Perform calculation: median of user-selected 
steadyTriplet(:,ind)=median(TripletPop(ssStartIndex(ind):pulseEndIndex(ind),ind));
steadySinglet(:,ind)=median(SingletPop(ssStartIndex(ind):pulseEndIndex(ind),ind));
steadyGround(:,ind)=median(GroundPop(ssStartIndex(ind):pulseEndIndex(ind),ind));

% %Normalize to probability PER CHROMOPHORE by dividing by chromophores
% steadyTriplet=steadyTriplet/n_Chromophores;
% steadySinglet=steadySinglet/n_Chromophores;
% steadyGround=steadyGround/n_Chromophores;

end

end %End function