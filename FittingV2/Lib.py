import os
from multiprocessing.pool import Pool
from os import listdir
from os.path import join
from pathlib import Path

import scipy.optimize
import numpy as np
import matplotlib.pyplot as plt


class Waveform:

    Name = ""
    Raw = None

    def __init__(self, name, timeseries, extractionPoints):
        self.Traces = []
        self.RawTraces = []
        self._Extract(timeseries, extractionPoints)
        self.Raw = timeseries
        self.Name = name


    def _Extract(self, timeseries, extractionPoints):
        for (start, end) in extractionPoints:
            self.Traces.append(np.copy(timeseries[start:end, :]))

    def Filter(self, filter):
        self.RawTraces = self.Traces
        self.Traces = list(map(lambda x: filter(np.copy(x)), self.Traces))




class Optimizer:
    def __init__(self, target, errorFunction, initalParams, method):
        self.Target = target
        self.ErrorFunction = errorFunction
        self.FixedParams = initalParams
        self.ModelDataList = None
        self.TotalError = 0
        self.Method = method

    def Run(self):
        def Opt(params):
            totalError = 0

            i = 0

            modelDataList = []

            for trace in self.Target.Traces:
                [error, modelData] = self.ErrorFunction(i, trace, params)
                modelDataList.append(modelData)
                totalError = totalError + error


                i += 1

            self.ModelDataList = modelDataList
            print(f"{self.Target.Name}: Total={totalError} Delta={totalError - self.TotalError}")
            self.TotalError = totalError



            return totalError




        self.Result = scipy.optimize.minimize(
            Opt, self.FixedParams, method="Nelder-Mead").x




def WaveformParser(fileParser, filter, extractionProvider):
    def Run(fileName, str):


        def Plot(traces, rawTraces):
            i = 1
            fig = plt.figure()


            for trace,rawTrace in zip(traces, rawTraces):
                sub = fig.add_subplot(1, len(traces), i)

                sub.plot(rawTrace[:, 0], rawTrace[:, 1])
                sub.plot(trace[:, 0], trace[:, 1])

                i = i + 1

            plt.tight_layout()
            fig.savefig(f"Filter/{Path(fileName).stem}.svg")
            plt.clf()


        [name, timeseries] = fileParser(fileName, str)

        waveform = Waveform(
            name, timeseries, extractionProvider(name, timeseries))



        waveform.Filter(filter)

        Plot(waveform.Traces, waveform.RawTraces)

        return waveform

    return Run

class Project:
    Name = ""
    TraceDirs = []
    Waveforms = []
    Optimizers = []


    def LoadWaveform(self, waveformParser, filePath):
        with open(filePath, "r") as file:
            waveform = waveformParser(filePath, file.read())
            self.Waveforms.append(waveform)


    def LoadWaveforms(self, waveformParser):
        for dir in self.TraceDirs:
            for filePath in listdir(dir):
                self.LoadWaveform(waveformParser, join(dir, filePath))


    def RunOptimization(self, errorFunction, initalParams, method = "Powell", parallel = True):
        pool = Pool(12)

        for waveform in self.Waveforms:
            self.Optimizers.append(Optimizer(
                waveform, errorFunction, initalParams, method))

        if parallel:
            self.Optimizers = pool.map(Run, self.Optimizers)
        else:
            for opt in self.Optimizers:
                Run(opt)



def Run(opt):
    opt.Run()

    return opt