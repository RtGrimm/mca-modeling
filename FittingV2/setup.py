import os
from distutils.core import setup
from Cython.Build import cythonize

os.environ['CFLAGS'] = '-Ofast -march=native -flto -ffast-math'

setup(
    ext_modules=cythonize("NativeMain.pyx"),

)