import numpy as np
from numpy.matlib import repmat

def RunSim(kexc = 1e6,
            kisc=1e9,
            kiscR=1.1e5,
            kst=5e8,
            ktt=1e9,
            tauFL = 600e-12,
            nMax = 20,
            iterNum = 500,
            tol = 1.0e-6,
            timestep = 1e-7,
            endTime = 6.5e-5):

    qMax = int(round(endTime / timestep))

    kf = np.zeros((nMax + 1, 1))
    kf.fill(kexc * kisc * tauFL)

    P = np.zeros((1, nMax + 1))
    P[0, 0] = 1
    tmp = repmat(np.append([0.99], np.linspace(1e-6, 1e-7, nMax)), qMax + 1, 1)

    P = np.append(P, tmp, axis=0)
    P = np.append(P, np.zeros((qMax + 2, 2)), axis=1)

    Pold = P.copy()[0:qMax + 2, 0:nMax + 1]

    b = np.zeros((qMax + 1, nMax + 1))

    theta = 0.5

    #err_Store = np.zeros((iterNum, qMax))

    for q in range(0, qMax):
        iteration = 1
        err = [100000]

        while iteration <= iterNum and max(err) > tol:
            for n in range(0, nMax + 1):
                if n - 1 < 0:
                    tmp_b = 0.0
                    tmp_P = 0.0
                else:
                    tmp_b = kf[n] * P[q, n - 1]
                    tmp_P = kf[n] * P[q + 1, n - 1]

                b_part1 = P[q, n] * ((1 / timestep) - (1 - theta) * (kf[n] + n * kiscR + n * (n - 1) * (ktt * 0.5)))
                b_part2 = (1 - theta) * (
                            tmp_b + kiscR * (n + 1) * P[q, n + 1] + (ktt * 0.5) * (n + 2) * (n + 1) * P[q, n + 2])
                b[q, n] = b_part1 + b_part2

                P_numerator = theta * (tmp_P + kiscR * (n + 1) * P[q + 1, n + 1] + (ktt * 0.5) * (n + 2) * (n + 1) * P[
                    q + 1, n + 2]) + b[q, n]
                P_denominator = (1 / timestep) + theta * (kf[n] + n * kiscR + n * (n - 1) * (ktt * 0.5))

                P[q + 1, n] = P_numerator / P_denominator

            err = abs((P[q + 1, 0:nMax + 1] - Pold[q + 1, :]) / P[q + 1, 0: nMax + 1])
            Pold = P.copy()[0:qMax + 2, 0: nMax + 1]

            #err_Store[iteration - 1, q] = max(err)
            iteration += 1

    Pfinal = P[0:qMax, 0: nMax]

    t = np.array(range(0, qMax)) * timestep
    t = t

    I = np.zeros((1, qMax))

    for n in range(0, nMax):
        I = I + (Pfinal[:, n] / (1 + (n * kst * tauFL)))

    return [t, I[0], Pfinal]