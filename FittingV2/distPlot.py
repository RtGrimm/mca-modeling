import re
from os import listdir
from os.path import join

import matplotlib
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sb
from matplotlib.pyplot import pause
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import curve_fit

from main import CalcKexc

def CleanName(name):
    return re.sub("""_trace_\d""", "", name)

def FitExp(x, y, count):
    def F(x, a, b, c, d, e, f):
        return (b*np.exp(a*x) + c) + (e*np.exp(d*x) + f)

    (a, b, c, d, e, f), pconv = curve_fit(F, x / np.max(x), y / np.max(y))



    xfit = np.linspace(np.min(x), np.max(x), count)
    return (xfit, F(xfit / np.max(x), a, b, c, d, e, f) * np.max(y))

def LoadTraces(dir, nameSet):
    for file in listdir(dir):
        if not (CleanName(file) in nameSet.values):
            continue

        path = join(dir, file)
        yield (file, np.loadtxt(path))




def CalcQD(pair, data):
    name, trace = pair

    index = 0

    for i in [1, 2, 3]:
        if f"trace_{i}" in name:
            index = i

    I = trace[:, 1]

    QD = I[0] / I[-1]
    n = data[data["name"] == CleanName(name)]["n"].values[0]

    return (CalcKexc(index - 1)*n, QD)

data = pd.read_csv("dist.csv")

QD = np.array(list(map(lambda x: CalcQD(x, data), LoadTraces("Traces", data["name"]))))

sb.set()
sb.set_context("talk")
plt.rcParams['font.family'] = 'serif'
plt.rcParams['mathtext.fontset'] = 'dejavuserif'

matplotlib.rcParams.update({'figure.figsize': (16, 9)})

plt.style.use("seaborn-deep")




binCount = 200



plt.clf()

TEx_1 = data["TEx_1"].values
TEx_2 = data["TEx_2"].values
TEx_3 = data["TEx_3"].values

n = data["n"]
error = data["Error"].values
kst = np.log10(data["kst"].values)
ktt = np.log10(data["ktt"].values)

binCount = 50

def PlotDist():
    fig = plt.figure(1)

    def PlotRateHist(data, name, title, xlabel, i):
        plt.subplot(1, 4, i)

        plt.title(title)
        plt.xlabel(xlabel)

        sb.distplot(data, bins=binCount)

    PlotRateHist(kst, "kst", "${k_{st}}$", "${log({k_{st}})}$", 1)
    PlotRateHist(ktt, "ktt", "${k_{tt}}$", "${log({k_{tt}})}$", 2)
    PlotRateHist(n, "n", "n", "n", 3)
    PlotRateHist(error, "error", "error", "error", 4)

    plt.tight_layout()

    fig.savefig(f"Figures/Dist.svg")
    fig.show()

PlotDist()

fig2 = plt.figure(2)


plt.ylabel("${<T_>}$")
plt.xlabel("${n}$")

plt.scatter(n, TEx_1, label="Pulse 1")
plt.scatter(n, TEx_2, label="Pulse 2")
plt.scatter(n, TEx_3, label="Pulse 3")

plt.legend()
fig2.show()
fig2.savefig("Figures/nT.svg")

fig3 = plt.figure(3)



plt.ylabel("${log({k})}$")
plt.xlabel("${n}$")
plt.scatter(n, ktt, label="ktt")
plt.scatter(n, kst, label="kst")
plt.legend()
fig3.show()
fig3.savefig("Figures/nK.svg")

fig4 = plt.figure(4)




plt.ylabel("${QD}$")
plt.xlabel("${k_{exc}}$")

plt.scatter(QD[:, 0], QD[:, 1])

fig4.show()
fig4.savefig("Figures/kexcQD.svg")


fig5 = plt.figure(5)




plt.subplot(1, 3, 1)
plt.ylabel("error")
plt.title("n")
plt.scatter(n, error)
plt.subplot(1, 3, 2)
plt.ylabel("error")
plt.title("${k_{st}}$")
plt.scatter(kst, error)
plt.subplot(1, 3, 3)
plt.ylabel("error")
plt.title("${k_{tt}}$")
plt.scatter(ktt, error)
plt.tight_layout()

fig5.show()
fig5.savefig("Figures/ErrorScatter.svg")


fig6 = plt.figure(6)
ax3d = Axes3D(fig6)

ax3d.scatter(ktt, kst, n)


fig7 = plt.figure(7)
plt.title("${<T_1>}$")

sb.distplot(TEx_1, bins=100, label="Pulse 1")
sb.distplot(TEx_2, bins=100, label="Pulse 2")
sb.distplot(TEx_3, bins=100, label="Pulse 3")

plt.legend()
fig7.savefig("Figures/TEx.svg")

plt.show()
