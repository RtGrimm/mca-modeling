import matplotlib.pyplot as plt
from numpy import linspace
from numpy.ma import dot

from Tachiya import RunSim


def CalcKexc(index):
    i = [
        # 20,
        95, 220, 370]

    sigma = 1e-15
    l = 0.0000568
    h = 6.6260755e-34
    c = 2.998e10

    return (i[index] * sigma * l) / (h * c)

exList = []
kexcList = []

for kexc in linspace(1e4, 1e6, 20):
    nMax = 20

    [t, I, PFinal] = RunSim(nMax=nMax, ktt=1.5309911260224206e-06, kst=9411164393.704432, kexc=kexc, endTime=10e-5)

    P = PFinal[-1, :]
    V = linspace(0, nMax - 1, nMax)

    tripletExpectation = dot(P, V)
    print(tripletExpectation)

    exList.append(tripletExpectation)
    kexcList.append(kexc)

plt.plot(kexcList, exList)
plt.show()

