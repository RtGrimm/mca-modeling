import signal
from io import StringIO
from io import StringIO
from itertools import takewhile
from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as la
import pandas as pd
from numpy.ma import argmax, clip
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter, argrelmax, argrelmin, medfilt

from Lib import Project, WaveformParser
from Tachiya import RunSim
import seaborn as sb
import pandas as pd

import scipy.signal as signal


import os
from multiprocessing.pool import Pool
from os import listdir
from os.path import join
from pathlib import Path

import scipy.optimize
import numpy as np
import matplotlib.pyplot as plt

import numpy as np
from numpy.matlib import repmat

def RunSim(kexc = 1e6,
            kisc=1e9,
            kiscR=1.1e5,
            kst=5e8,
            ktt=1e9,
            tauFL = 600e-12,
            nMax = 20,
            iterNum = 500,
            tol = 1.0e-6,
            timestep = 1e-7,
            endTime = 6.5e-5):

    qMax = int(round(endTime / timestep))

    kf = np.zeros((nMax + 1, 1))
    kf.fill(kexc * kisc * tauFL)

    P = np.zeros((1, nMax + 1))
    P[0, 0] = 1
    tmp = repmat(np.append([0.99], np.linspace(1e-6, 1e-7, nMax)), qMax + 1, 1)

    P = np.append(P, tmp, axis=0)
    P = np.append(P, np.zeros((qMax + 2, 2)), axis=1)

    Pold = P.copy()[0:qMax + 2, 0:nMax + 1]

    b = np.zeros((qMax + 1, nMax + 1))

    theta = 0.5

    #err_Store = np.zeros((iterNum, qMax))

    for q in range(0, qMax):
        iteration = 1
        err = [100000]

        while iteration <= iterNum and max(err) > tol:
            for n in range(0, nMax + 1):
                if n - 1 < 0:
                    tmp_b = 0.0
                    tmp_P = 0.0
                else:
                    tmp_b = kf[n] * P[q, n - 1]
                    tmp_P = kf[n] * P[q + 1, n - 1]

                b_part1 = P[q, n] * ((1 / timestep) - (1 - theta) * (kf[n] + n * kiscR + n * (n - 1) * (ktt * 0.5)))
                b_part2 = (1 - theta) * (
                            tmp_b + kiscR * (n + 1) * P[q, n + 1] + (ktt * 0.5) * (n + 2) * (n + 1) * P[q, n + 2])
                b[q, n] = b_part1 + b_part2

                P_numerator = theta * (tmp_P + kiscR * (n + 1) * P[q + 1, n + 1] + (ktt * 0.5) * (n + 2) * (n + 1) * P[
                    q + 1, n + 2]) + b[q, n]
                P_denominator = (1 / timestep) + theta * (kf[n] + n * kiscR + n * (n - 1) * (ktt * 0.5))

                P[q + 1, n] = P_numerator / P_denominator

            err = abs((P[q + 1, 0:nMax + 1] - Pold[q + 1, :]) / P[q + 1, 0: nMax + 1])
            Pold = P.copy()[0:qMax + 2, 0: nMax + 1]

            #err_Store[iteration - 1, q] = max(err)
            iteration += 1

    Pfinal = P[0:qMax, 0: nMax]

    t = np.array(range(0, qMax)) * timestep
    t = t

    I = np.zeros((1, qMax))

    for n in range(0, nMax):
        I = I + (Pfinal[:, n] / (1 + (n * kst * tauFL)))

    return [t, I[0], Pfinal]

class Waveform:

    Name = ""
    Raw = None

    def __init__(self, name, timeseries, extractionPoints):
        self.Traces = []
        self.RawTraces = []
        self._Extract(timeseries, extractionPoints)
        self.Raw = timeseries
        self.Name = name


    def _Extract(self, timeseries, extractionPoints):
        for (start, end) in extractionPoints:
            self.Traces.append(np.copy(timeseries[start:end, :]))

    def Filter(self, filter):
        self.RawTraces = self.Traces
        self.Traces = list(map(lambda x: filter(np.copy(x)), self.Traces))




class Optimizer:
    def __init__(self, target, errorFunction, initalParams, method):
        self.Target = target
        self.ErrorFunction = errorFunction
        self.FixedParams = initalParams
        self.ModelDataList = None
        self.TotalError = 0
        self.Method = method

    def Run(self):
        def Opt(params):
            totalError = 0

            i = 0

            modelDataList = []

            for trace in self.Target.Traces:
                [error, modelData] = self.ErrorFunction(i, trace, params)
                modelDataList.append(modelData)
                totalError = totalError + error


                i += 1

            self.ModelDataList = modelDataList
            print(f"{self.Target.Name}: Total={totalError} Delta={totalError - self.TotalError}")
            self.TotalError = totalError



            return totalError




        self.Result = scipy.optimize.minimize(
            Opt, self.FixedParams, method="Nelder-Mead").x




def WaveformParser(fileParser, filter, extractionProvider):
    def Run(fileName, str):


        def Plot(traces, rawTraces):
            i = 1
            fig = plt.figure()


            for trace,rawTrace in zip(traces, rawTraces):
                sub = fig.add_subplot(1, len(traces), i)

                sub.plot(rawTrace[:, 0], rawTrace[:, 1])
                sub.plot(trace[:, 0], trace[:, 1])

                i = i + 1

            plt.tight_layout()
            fig.savefig(f"Filter/{Path(fileName).stem}.svg")
            plt.clf()


        [name, timeseries] = fileParser(fileName, str)

        waveform = Waveform(
            name, timeseries, extractionProvider(name, timeseries))



        waveform.Filter(filter)

        Plot(waveform.Traces, waveform.RawTraces)

        return waveform

    return Run

class Project:
    Name = ""
    TraceDirs = []
    Waveforms = []
    Optimizers = []


    def LoadWaveform(self, waveformParser, filePath):
        with open(filePath, "r") as file:
            waveform = waveformParser(filePath, file.read())
            self.Waveforms.append(waveform)


    def LoadWaveforms(self, waveformParser):
        for dir in self.TraceDirs:
            for filePath in listdir(dir):
                self.LoadWaveform(waveformParser, join(dir, filePath))


    def RunOptimization(self, errorFunction, initalParams, method = "Powell", parallel = True):
        pool = Pool(12)

        for waveform in self.Waveforms:
            self.Optimizers.append(Optimizer(
                waveform, errorFunction, initalParams, method))

        if parallel:
            self.Optimizers = pool.map(Run, self.Optimizers)
        else:
            for opt in self.Optimizers:
                Run(opt)



def Run(opt):
    opt.Run()

    return opt


sb.set()
matplotlib.rcParams.update({'font.size': 14})

timestep = 1e-7
nMax = 20


def LoadASC(file, ts):
    lines = [line for line in file.read().split("\n") if len(line) > 0]

    t = np.array(range(0, len(lines))) * ts
    I = np.array(list(map(float, lines)))

    return np.column_stack((t, I))


def ParseCSVWaveform(fileName, str):
    timeseries = LoadASC(StringIO(str), timestep)
    return [Path(fileName).stem.replace(" ", "_"), timeseries]

def FilterLowPass(xs):
    b, a = signal.butter(2, 0.025, 'low')
    return signal.filtfilt(b, a, xs)

def FilterSavGol(xs):
    xs = savgol_filter(xs, 51, 1)
    return xs


def FilterInterp(xs):
    f = interp1d(xs[:, 0], xs[:, 1], fill_value="extrapolate")
    newTimes = xs[:, 0][::3]

    return FilterRolling(np.row_stack((newTimes.transpose(), f(newTimes))).transpose())

def FilterRolling(timeseries):
    n = 30

    #pdSeries = pd.Series(timeseries[:, 1], timeseries[:, 0])
    #x = pdSeries.rolling(n).mean().values

    #x[:n] = timeseries[:n][:, 1]

    x = FilterSavGol(FilterLowPass(timeseries[:, 1]))

    xorg = timeseries[:, 1]
    #x = savgol_filter(timeseries[:, 1], 151, 1)
    t = timeseries[:, 0]
    tnorm = (t - np.min(t)) / np.max(t)
    blend = (1 - np.exp(-tnorm * 3e2))
    blendTrace = (1 - blend) * xorg + x * blend





    #plt.plot(xorg)
    #plt.plot(blendTrace)
    #plt.show()

    return np.column_stack((t, blendTrace))


def FilterID(xs):

    return xs


def LeftMaxExtractionPoints(irfPath):
    with open(irfPath, "r") as file:
        IRF = LoadASC(file, 1e-7)

    IRF[:, 1] = IRF[:, 1] / np.max(IRF[:, 1])

    grad = np.power(np.gradient(IRF[:, 1]), 1)

    irfMax = argrelmax(grad, order=1200)[0][1:4]
    irfMin = argrelmin(grad, order=1200)[0][1:4]

    def Run(name, timeseries):
        offsetLeft = 30
        offsetRight = 40

        min = irfMin - offsetRight
        max = irfMax + offsetLeft

        window = 100

        def CorrectLeft(index):
            start = np.max((index - window, 0))

            searchWindow = timeseries[:, 1][start:index]
            return start + argmax(searchWindow)

        max = np.array(list(map(CorrectLeft, max)))

        return [(M, m) for m, M in zip(min, max)]

    return Run


def CalcKexc(index):
    power53 = [46.18, 85.09, 119.04, 159.66]
    power100 = [9.54288533,  36.45136458, 139.70292648, 433.11593303]

    i = power53[1:4]

    sigma = 1e-15
    l = 0.0000568
    h = 6.6260755e-34
    c = 2.998e10
    return (i[index] * sigma * l) / (h * c)


def ErrorFunction(traceIndex, trace, params):
    [ktt, kst, n] = params



    startTime = np.min(trace[:, 0])
    endTime = np.max(trace[:, 0])

    kexc = CalcKexc(traceIndex)

    [tModel, IModel, PFinal] = RunSim(
        ktt=abs(ktt), kst=abs(kst), kexc=n*kexc, endTime=(endTime - startTime), nMax=nMax)

    tExp, IExp = trace[:, 0], trace[:, 1]
    iMax = np.max(IExp)

    IExp = IExp / np.max(IExp)

    f = interp1d(tModel, IModel, fill_value="extrapolate")

    matchPoints = f(tExp - startTime)

    csExp = np.cumsum(IExp)
    csModel = np.cumsum(matchPoints)
    error = np.power(la.norm(csExp - csModel), 2) / len(csExp)

    #plt.plot(csExp)
    #plt.plot(csModel)
    #plt.savefig(f"FitImages/{traceIndex}.svg")
    #plt.clf()


    P = PFinal[-1, :]

    ptotal = np.sum(P)
    if abs(ptotal - 1) > 0.01:
        print(f"{traceIndex} - Ptotal = {ptotal}")
        error = np.inf

    if error == np.nan:
        error = np.inf



    return [error, {
        "Trace": np.array((tExp, matchPoints * iMax)).transpose(),
        "P": PFinal
    }]


def PlotFits(proj):
    for opt in proj.Optimizers:
        waveform = opt.Target

        plt.plot(waveform.Raw[:, 0], waveform.Raw[:, 1])

        for data in opt.ModelDataList:
            modelTrace = data["Trace"]

            plt.plot(modelTrace[:, 0], modelTrace[:, 1])

        plt.savefig(f"Plots/{waveform.Name}.svg")
        plt.clf()


def WriteDist(proj):
    with open("dist.csv", mode="w") as file:
        file.write("name,ktt,kst,n,TEx_1,TEx_2,TEx_3,P_1,P_2,P_3,Error\n")

        for opt in proj.Optimizers:
            ktt, kst, n = opt.Result

            TExList = []
            PList = []

            for data in opt.ModelDataList:
                P = data["P"][-1, :]

                V = np.linspace(0, nMax - 1, nMax)
                TExList.append(np.dot(P, V))
                PList.append(np.sum(P))

            TEx_1 = TExList[0]
            TEx_2 = TExList[1]
            TEx_3 = TExList[2]

            file.write(f"{opt.Target.Name},{abs(ktt)},{abs(kst)},{n},{TEx_1},{TEx_2},{TEx_3},{PList[0]},{PList[1]},{PList[2]},{opt.TotalError}\n")

def WriteTraces(proj):
    for opt in proj.Optimizers:
        i = 1

        for data in opt.ModelDataList:
            modelTrace = data["Trace"]
            np.savetxt(f"Traces/{opt.Target.Name}_trace_{str(i)}", modelTrace)

            i += 1


def RunMain():
    project = Project()

    targetDir = "Projects/53nWMultiVar"

    project.TraceDirs = [
        f"{targetDir}/Traces"
    ]

    project.LoadWaveforms(
        WaveformParser(ParseCSVWaveform, FilterID,
                       LeftMaxExtractionPoints(f"{targetDir}/IRF.asc")),
    )

    project.RunOptimization(ErrorFunction, [1e4, 5e8, 1], parallel=True)

    PlotFits(project)
    WriteDist(project)
    WriteTraces(project)

def RunNative():
    RunMain()