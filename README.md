
Projects
========

Dave Walwark
------------

### DaveKMC

The original implementation of the KMC(Kinetic Monte Carlo) written in MATLAB

### DaveKMCTranspile

A modified version of the original KMC that is transpiled to C++ for increased performance

Ryan Grimm
----------

### RyanKMC(Native/Py)

A rewrite of DaveKMC intended to provide a more generic implementation of the KMC 

The project consists of two parts: a "reference" implementation(RyanKMCPy) written in Python for prototyping and a parallelized version written in C++.

Ryan Grimm & Ben Datko
----------------------

### ODEMCA

A simple demonstration of solving a rate equation model using numerical methods
