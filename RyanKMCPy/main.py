import numpy as np
import matplotlib.pyplot as plt

def DuplicateCount(x, xs):
    n = -1

    for y in xs:
        if x == y:
            n += 1

    return n


class ReactionElement:
    def __init__(self, name, n0=0):
        self.name = name
        self.n = n0
        self.history = []

    def Update(self, t, n):
        self.history.append(t)
        self.n = n


class Reaction:
    def __init__(self, rate, reactants, products):
        self.rate = rate
        self.reactants = reactants
        self.products = products

    def PopProduct(self):
        product = 1

        for reactant in self.reactants:
            dc = DuplicateCount(reactant, self.reactants)
            product = product * (reactant.n - dc) * (1.0 / (float(dc + 1)))

        return product

    def Run(self, t):
        for reactant in self.reactants:
            reactant.Update(t, reactant.n - 1)

        for product in self.products:
            product.Update(t, product.n + 1)


class Simulation:
    def __init__(self, reactions):
        self.reactions = reactions
        self.t = 0

    def ComputeRand(self):
        return np.random.ranf()

    def DrawTime(self, wtotal):
        return (-1 / wtotal) * np.log(self.ComputeRand())

    def Run(self, maxTime):
        while self.t < maxTime:
            rateList = np.zeros(len(self.reactions))

            wtotal = 0
            i = 0
            for reaction in self.reactions:
                wtotal += reaction.PopProduct() * reaction.rate
                rateList[i] = wtotal

                i += 1

            r = self.ComputeRand() * wtotal

            (reactionI,) = np.nonzero(rateList > r)

            if len(reactionI) > 0:
                self.reactions[reactionI[0]].Run(self.t)

            dt = self.DrawTime(wtotal)

            self.t += dt


num_chromphers = 12
maxTime = 5e-5
kexc = 1e6
kisc = 1e9
kiscR = 1e5
tauFL = 500e-12
kst = 1e8
ktt = 1e5


Itotal = []

for n in range(0, 1000):
    S0 = ReactionElement("S0", num_chromphers)
    S1 = ReactionElement("S1")
    T1 = ReactionElement("T1")
    hv = ReactionElement("Photons")

    exc = Reaction(kexc, [S0], [S1])
    isc = Reaction(kisc, [S1], [T1])
    iscR = Reaction(kiscR, [T1], [S0])
    fl = Reaction(1 / tauFL, [S1], [S0, hv])
    st = Reaction(kst, [S1, T1], [S0, T1])
    tt = Reaction(ktt, [T1, T1], [S0, S0])

    sim = Simulation([exc, isc, iscR, fl, st, tt])
    sim.Run(maxTime)

    pairs = np.array(hv.history)
    Itotal += hv.history

    print(n)




plt.xlim(0, maxTime)

plt.hist(Itotal, bins=1000)
plt.show()