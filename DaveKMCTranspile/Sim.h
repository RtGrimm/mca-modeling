#pragma once

#include <vector>



class Sim {
public:
    std::vector<double> PhotonCount;
    std::vector<double> TimeBasis;

    void Run(
            double Kfl, double Kisc, double Krisc, double Kss, double Kst,
            double Ktt, double Kph, double Ksf, double kexc, double
            desiredBinSize, double MCAcycles, double Nchromophores);
};

