/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim.c
 *
 * Code generation for function 'RunSim'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "RunSim_emxutil.h"
#include "MCa_Feb1.h"
#include "rand.h"

/* Function Definitions */
void RunSim(double Kfl, double Kisc, double Krisc, double Kss, double Kst,
            double Ktt, double Kph, double Ksf, double kexc, double
            desiredBinSize, double MCAcycles, double Nchromophores,
            emxArray_real_T *photonCount_out, emxArray_real_T *timeBasis_out)
{
  double numberBins;
  double MCA_experimentConfig[4];
  static double randoms[10000000];
  int k;
  emxArray_real_T *PhotonCts;
  int i0;
  emxArray_real_T *r0;
  emxArray_real_T *r1;
  emxArray_real_T *r2;
  emxArray_real_T *timeBasis;
  emxArray_real_T *r3;
  emxArray_real_T *r4;
  emxArray_real_T *r5;
  emxArray_real_T *r6;
  emxArray_real_T *r7;
  emxArray_real_T *r8;
  double b_Kfl[8];
  int loop_ub;
  emxArray_real_T *y;

  /* Run-script for MCA_MC function -D. J. Walwark 2019 */
  /* Molecular Configuration */
  /* ===================================================== */
  /*  Estimated quantum yield: */
  /* MCA Experimental Configuration */
  /* ===================================================== */
  /*  How many MCA cycles to perform */
  /*  This is the most important parameter for determining simulation runtime. */
  /* How long is pulse on during min excitation? (the longest pulse time) */
  /* Time each stairstep takes up (including pulse-off time) */
  /*  % Ratio of adjacent steps (old Grey-group data isn't this simple, since it */
  /*  % is usually 0.25, 0.5, 0.75, 1, which isn't a constant ratio ) */
  /* How to scale the time as a function of power */
  /*  Closer to 1/2 or 1 makes high-excitation pulse-time very short compared to */
  /*  low-excitation pulse-time. */
  /*  Closer to ~0 makes high-excitation-pulse-time same as low excitation */
  /* ------------------------------ */
  /* Configure stairstep heights and pulse time stretching explicitly here */
  /* Ad-hoc parameter space scaling */
  /* Use 3/4 for getting to the knee in linspace. */
  /* A linear space for lower intensity excitation */
  /* Apply quantum efficiency scaling */
  /*  Set up the perfect experiment, with more pulse time for lower excitations */
  /*  pulseTimeVec = []; */
  /*  pulseTimeVec = pulseTimeRatioList*pulseTimeAtExcMin; */
  /* This many time-bins total */
  numberBins = floor(0.00101 / desiredBinSize);

  /*  MCA bins to use across 'maxTime'. Use 'numberBins' or circumvent as needed. */
  /* Normalize by chromophores to make run-times more consistent */
  MCAcycles /= sqrt(Nchromophores);
  MCAcycles = ceil(MCAcycles);
  if (MCAcycles < 1.0) {
    MCAcycles = 1.0;
  }

  MCA_experimentConfig[0] = 0.001;
  MCA_experimentConfig[1] = 0.00101;
  MCA_experimentConfig[2] = numberBins;
  MCA_experimentConfig[3] = MCAcycles;

  /* ------------------------------------------------------- */
  /* Load em up */
  /*  Execution of Monte Carlo with above settings */
  /* Random number generation and reusable stockpile */
  /*  It is nice for debugging to have a random number list that doesn't change */
  /*  until you want it to. Use the 'if' statement to hold list constant. */
  /* Length of random number list. It has 2 columns. */
  /* for choosing which rxn */
  b_rand(randoms);

  /* Necessary logarithm for time advance */
  for (k = 0; k < 5000000; k++) {
    randoms[5000000 + k] = log(randoms[5000000 + k]);
  }

  /* If you set MCAcycles to 1 you automatically get debug mode */
  emxInit_real_T(&PhotonCts, 1);

  /* Initialize output of simulation matrices */
  i0 = PhotonCts->size[0];
  k = (int)numberBins;
  PhotonCts->size[0] = k;
  emxEnsureCapacity_real_T(PhotonCts, i0);
  for (i0 = 0; i0 < k; i0++) {
    PhotonCts->data[i0] = 0.0;
  }

  emxInit_real_T(&r0, 1);
  emxInit_real_T(&r1, 1);
  emxInit_real_T(&r2, 1);
  emxInit_real_T(&timeBasis, 1);
  emxInit_real_T(&r3, 1);
  emxInit_real_T(&r4, 1);
  emxInit_real_T(&r5, 1);
  emxInit_real_T(&r6, 1);
  emxInit_real_T(&r7, 1);
  emxInit_real_T(&r8, 1);

  /*  profile on */
  b_Kfl[0] = Kfl;
  b_Kfl[1] = Kisc;
  b_Kfl[2] = Krisc;
  b_Kfl[3] = Kss;
  b_Kfl[4] = Kst;
  b_Kfl[5] = Ktt;
  b_Kfl[6] = Kph;
  b_Kfl[7] = Ksf;
  MCa_Feb1(randoms, Nchromophores, kexc * 0.5, MCA_experimentConfig, b_Kfl, r0,
           r1, r2, timeBasis, r3, r4, r5, r6, r7, r8);
  loop_ub = r3->size[0];
  emxFree_real_T(&r8);
  emxFree_real_T(&r7);
  emxFree_real_T(&r6);
  emxFree_real_T(&r5);
  emxFree_real_T(&r4);
  emxFree_real_T(&timeBasis);
  emxFree_real_T(&r2);
  emxFree_real_T(&r1);
  emxFree_real_T(&r0);
  for (i0 = 0; i0 < loop_ub; i0++) {
    PhotonCts->data[i0] = r3->data[i0];
  }

  emxFree_real_T(&r3);
  loop_ub = PhotonCts->size[0];
  i0 = photonCount_out->size[0];
  photonCount_out->size[0] = loop_ub;
  emxEnsureCapacity_real_T(photonCount_out, i0);
  for (i0 = 0; i0 < loop_ub; i0++) {
    photonCount_out->data[i0] = PhotonCts->data[i0];
  }

  emxFree_real_T(&PhotonCts);
  emxInit_real_T(&y, 2);

  /* What will be used for plotting */
  i0 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = k;
  emxEnsureCapacity_real_T(y, i0);
  if (y->size[1] >= 1) {
    y->data[y->size[1] - 1] = 0.00101;
    if (y->size[1] >= 2) {
      y->data[0] = 0.0;
      if (y->size[1] >= 3) {
        numberBins = 0.00101 / ((double)y->size[1] - 1.0);
        i0 = y->size[1];
        for (k = 0; k <= i0 - 3; k++) {
          y->data[k + 1] = (1.0 + (double)k) * numberBins;
        }
      }
    }
  }

  i0 = timeBasis_out->size[0];
  timeBasis_out->size[0] = y->size[1];
  emxEnsureCapacity_real_T(timeBasis_out, i0);
  k = y->size[1];
  for (i0 = 0; i0 < k; i0++) {
    timeBasis_out->data[i0] = y->data[i0];
  }

  emxFree_real_T(&y);
}

/* End of code generation (RunSim.c) */
