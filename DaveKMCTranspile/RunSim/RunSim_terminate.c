/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim_terminate.c
 *
 * Code generation for function 'RunSim_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "RunSim_terminate.h"

/* Function Definitions */
void RunSim_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (RunSim_terminate.c) */
