/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * bsearch.h
 *
 * Code generation for function 'bsearch'
 *
 */

#ifndef BSEARCH_H
#define BSEARCH_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern int b_bsearch(const emxArray_real_T *x, double xi);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (bsearch.h) */
