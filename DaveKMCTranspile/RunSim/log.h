/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * log.h
 *
 * Code generation for function 'log'
 *
 */

#ifndef LOG_H
#define LOG_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void b_log(double x[5000000]);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (log.h) */
