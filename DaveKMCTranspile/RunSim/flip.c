/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flip.c
 *
 * Code generation for function 'flip'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "flip.h"

/* Function Definitions */
void flip(double x[10000000])
{
  int j;
  int offset;
  int k;
  int tmp_tmp;
  double tmp;
  int i6;
  for (j = 0; j < 2; j++) {
    offset = j * 5000000 + 5000000;
    for (k = 0; k < 2500000; k++) {
      tmp_tmp = (offset + k) - 5000000;
      tmp = x[tmp_tmp];
      i6 = (offset - k) - 1;
      x[tmp_tmp] = x[i6];
      x[i6] = tmp;
    }
  }
}

/* End of code generation (flip.c) */
