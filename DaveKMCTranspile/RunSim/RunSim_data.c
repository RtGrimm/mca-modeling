/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim_data.c
 *
 * Code generation for function 'RunSim_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "RunSim_data.h"

/* Variable Definitions */
unsigned int state[625];

/* End of code generation (RunSim_data.c) */
