/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * log.c
 *
 * Code generation for function 'log'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "log.h"

/* Function Definitions */
void b_log(double x[5000000])
{
  int k;
  for (k = 0; k < 5000000; k++) {
    x[k] = log(x[k]);
  }
}

/* End of code generation (log.c) */
