/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_RunSim_info.h
 *
 * Code generation for function 'RunSim'
 *
 */

#ifndef _CODER_RUNSIM_INFO_H
#define _CODER_RUNSIM_INFO_H
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#endif
/* End of code generation (_coder_RunSim_info.h) */
