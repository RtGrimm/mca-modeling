/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_RunSim_api.c
 *
 * Code generation for function '_coder_RunSim_api'
 *
 */

/* Include files */
#include "tmwtypes.h"
#include "_coder_RunSim_api.h"
#include "_coder_RunSim_mex.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131467U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "RunSim",                            /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

/* Function Declarations */
static real_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static real_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *Kfl, const
  char_T *identifier);
static const mxArray *emlrt_marshallOut(const emxArray_real_T *u);
static void emxFree_real_T(emxArray_real_T **pEmxArray);
static void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, boolean_T doPush);

/* Function Definitions */
static real_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = c_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static real_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *Kfl, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(Kfl), &thisId);
  emlrtDestroyArray(&Kfl);
  return y;
}

static const mxArray *emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m0;
  static const int32_T iv0[1] = { 0 };

  y = NULL;
  m0 = emlrtCreateNumericArray(1, iv0, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m0, &u->data[0]);
  emlrtSetDimensions((mxArray *)m0, u->size, 1);
  emlrtAssign(&y, m0);
  return y;
}

static void emxFree_real_T(emxArray_real_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_real_T *)NULL) {
    if (((*pEmxArray)->data != (real_T *)NULL) && (*pEmxArray)->canFreeData) {
      emlrtFreeMex((*pEmxArray)->data);
    }

    emlrtFreeMex((*pEmxArray)->size);
    emlrtFreeMex(*pEmxArray);
    *pEmxArray = (emxArray_real_T *)NULL;
  }
}

static void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, boolean_T doPush)
{
  emxArray_real_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_real_T *)emlrtMallocMex(sizeof(emxArray_real_T));
  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_real_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (real_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * numDimensions);
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

void RunSim_api(const mxArray * const prhs[12], int32_T nlhs, const mxArray
                *plhs[2])
{
  emxArray_real_T *photonCount_out;
  emxArray_real_T *timeBasis_out;
  real_T Kfl;
  real_T Kisc;
  real_T Krisc;
  real_T Kss;
  real_T Kst;
  real_T Ktt;
  real_T Kph;
  real_T Ksf;
  real_T kexc;
  real_T desiredBinSize;
  real_T MCAcycles;
  real_T Nchromophores;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_real_T(&st, &photonCount_out, 1, true);
  emxInit_real_T(&st, &timeBasis_out, 1, true);

  /* Marshall function inputs */
  Kfl = emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "Kfl");
  Kisc = emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "Kisc");
  Krisc = emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "Krisc");
  Kss = emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "Kss");
  Kst = emlrt_marshallIn(&st, emlrtAliasP(prhs[4]), "Kst");
  Ktt = emlrt_marshallIn(&st, emlrtAliasP(prhs[5]), "Ktt");
  Kph = emlrt_marshallIn(&st, emlrtAliasP(prhs[6]), "Kph");
  Ksf = emlrt_marshallIn(&st, emlrtAliasP(prhs[7]), "Ksf");
  kexc = emlrt_marshallIn(&st, emlrtAliasP(prhs[8]), "kexc");
  desiredBinSize = emlrt_marshallIn(&st, emlrtAliasP(prhs[9]), "desiredBinSize");
  MCAcycles = emlrt_marshallIn(&st, emlrtAliasP(prhs[10]), "MCAcycles");
  Nchromophores = emlrt_marshallIn(&st, emlrtAliasP(prhs[11]), "Nchromophores");

  /* Invoke the target function */
  RunSim(Kfl, Kisc, Krisc, Kss, Kst, Ktt, Kph, Ksf, kexc, desiredBinSize,
         MCAcycles, Nchromophores, photonCount_out, timeBasis_out);

  /* Marshall function outputs */
  photonCount_out->canFreeData = false;
  plhs[0] = emlrt_marshallOut(photonCount_out);
  emxFree_real_T(&photonCount_out);
  if (nlhs > 1) {
    timeBasis_out->canFreeData = false;
    plhs[1] = emlrt_marshallOut(timeBasis_out);
  }

  emxFree_real_T(&timeBasis_out);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

void RunSim_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  RunSim_xil_terminate();
}

void RunSim_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

void RunSim_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (_coder_RunSim_api.c) */
