/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_RunSim_api.h
 *
 * Code generation for function '_coder_RunSim_api'
 *
 */

#ifndef _CODER_RUNSIM_API_H
#define _CODER_RUNSIM_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_RunSim_api.h"

/* Type Definitions */
#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void RunSim(real_T Kfl, real_T Kisc, real_T Krisc, real_T Kss, real_T
                     Kst, real_T Ktt, real_T Kph, real_T Ksf, real_T kexc,
                     real_T desiredBinSize, real_T MCAcycles, real_T
                     Nchromophores, emxArray_real_T *photonCount_out,
                     emxArray_real_T *timeBasis_out);
  extern void RunSim_api(const mxArray * const prhs[12], int32_T nlhs, const
    mxArray *plhs[2]);
  extern void RunSim_atexit(void);
  extern void RunSim_initialize(void);
  extern void RunSim_terminate(void);
  extern void RunSim_xil_terminate(void);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (_coder_RunSim_api.h) */
