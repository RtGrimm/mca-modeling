/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * des.h
 *
 * Code generation for function 'des'
 *
 */

#ifndef DES_H
#define DES_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void des(const emxArray_real_T *x, const emxArray_real_T *edges,
                  emxArray_real_T *bins);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (des.h) */
