/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * linspace.h
 *
 * Code generation for function 'linspace'
 *
 */

#ifndef LINSPACE_H
#define LINSPACE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void linspace(double d2, double n1, emxArray_real_T *y);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (linspace.h) */
