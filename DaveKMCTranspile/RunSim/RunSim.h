/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim.h
 *
 * Code generation for function 'RunSim'
 *
 */

#ifndef RUNSIM_H
#define RUNSIM_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void RunSim(double Kfl, double Kisc, double Krisc, double Kss, double
                     Kst, double Ktt, double Kph, double Ksf, double kexc,
                     double desiredBinSize, double MCAcycles, double
                     Nchromophores, emxArray_real_T *photonCount_out,
                     emxArray_real_T *timeBasis_out);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (RunSim.h) */
