/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * histcounts.h
 *
 * Code generation for function 'histcounts'
 *
 */

#ifndef HISTCOUNTS_H
#define HISTCOUNTS_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void histcounts(const emxArray_real_T *x, const emxArray_real_T
    *varargin_1, emxArray_real_T *n);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (histcounts.h) */
