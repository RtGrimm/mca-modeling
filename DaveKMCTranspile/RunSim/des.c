/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * des.c
 *
 * Code generation for function 'des'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "des.h"
#include "RunSim_emxutil.h"
#include "bsearch.h"

/* Function Definitions */
void des(const emxArray_real_T *x, const emxArray_real_T *edges, emxArray_real_T
         *bins)
{
  emxArray_int32_T *bin;
  int nx;
  int k;
  double leftEdge;
  double delta;
  double bGuess;
  emxInit_int32_T(&bin, 1);
  nx = x->size[0];
  k = bin->size[0];
  bin->size[0] = nx;
  emxEnsureCapacity_int32_T(bin, k);
  for (k = 0; k < nx; k++) {
    bin->data[k] = 0;
  }

  nx = x->size[0];
  leftEdge = edges->data[0];
  delta = edges->data[1] - edges->data[0];
  for (k = 0; k < nx; k++) {
    if ((x->data[k] >= leftEdge) && (x->data[k] <= edges->data[edges->size[1] -
         1])) {
      bGuess = ceil((x->data[k] - leftEdge) / delta);
      if ((bGuess >= 1.0) && (bGuess < edges->size[1]) && (x->data[k] >=
           edges->data[(int)bGuess - 1]) && (x->data[k] < edges->data[(int)
           bGuess])) {
        bin->data[k] = (int)bGuess;
      } else {
        bin->data[k] = b_bsearch(edges, x->data[k]);
      }
    }
  }

  k = bins->size[0];
  bins->size[0] = bin->size[0];
  emxEnsureCapacity_real_T(bins, k);
  nx = bin->size[0];
  for (k = 0; k < nx; k++) {
    bins->data[k] = bin->data[k];
  }

  emxFree_int32_T(&bin);

  /* [bins, edges] = discretize(x, edges); */
}

/* End of code generation (des.c) */
