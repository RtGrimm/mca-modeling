/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim_initialize.h
 *
 * Code generation for function 'RunSim_initialize'
 *
 */

#ifndef RUNSIM_INITIALIZE_H
#define RUNSIM_INITIALIZE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void RunSim_initialize(void);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (RunSim_initialize.h) */
