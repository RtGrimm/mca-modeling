/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * histcounts.c
 *
 * Code generation for function 'histcounts'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "histcounts.h"
#include "RunSim_emxutil.h"
#include "bsearch.h"

/* Function Definitions */
void histcounts(const emxArray_real_T *x, const emxArray_real_T *varargin_1,
                emxArray_real_T *n)
{
  emxArray_int32_T *ni;
  int i4;
  int b;
  int nx;
  double leftEdge;
  double delta;
  int k;
  double bGuess;
  boolean_T guard1 = false;
  emxInit_int32_T(&ni, 2);
  i4 = ni->size[0] * ni->size[1];
  ni->size[0] = 1;
  ni->size[1] = varargin_1->size[1] - 1;
  emxEnsureCapacity_int32_T(ni, i4);
  b = varargin_1->size[1] - 1;
  for (i4 = 0; i4 < b; i4++) {
    ni->data[i4] = 0;
  }

  nx = x->size[0];
  leftEdge = varargin_1->data[0];
  delta = varargin_1->data[1] - varargin_1->data[0];
  for (k = 0; k < nx; k++) {
    if ((x->data[k] >= leftEdge) && (x->data[k] <= varargin_1->data
         [varargin_1->size[1] - 1])) {
      bGuess = ceil((x->data[k] - leftEdge) / delta);
      guard1 = false;
      if ((bGuess >= 1.0) && (bGuess < varargin_1->size[1])) {
        i4 = (int)bGuess - 1;
        if ((x->data[k] >= varargin_1->data[i4]) && (x->data[k] <
             varargin_1->data[(int)bGuess])) {
          ni->data[i4]++;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1) {
        b = b_bsearch(varargin_1, x->data[k]) - 1;
        ni->data[b]++;
      }
    }
  }

  i4 = n->size[0] * n->size[1];
  n->size[0] = 1;
  n->size[1] = ni->size[1];
  emxEnsureCapacity_real_T(n, i4);
  b = ni->size[0] * ni->size[1];
  for (i4 = 0; i4 < b; i4++) {
    n->data[i4] = ni->data[i4];
  }

  emxFree_int32_T(&ni);
}

/* End of code generation (histcounts.c) */
