/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * linspace.c
 *
 * Code generation for function 'linspace'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "linspace.h"
#include "RunSim_emxutil.h"

/* Function Definitions */
void linspace(double d2, double n1, emxArray_real_T *y)
{
  int i3;
  double delta1;
  int k;
  if (n1 < 0.0) {
    n1 = 0.0;
  }

  i3 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = (int)floor(n1);
  emxEnsureCapacity_real_T(y, i3);
  if (y->size[1] >= 1) {
    y->data[y->size[1] - 1] = d2;
    if (y->size[1] >= 2) {
      y->data[0] = 0.0;
      if (y->size[1] >= 3) {
        if ((d2 < 0.0) && (fabs(d2) > 8.9884656743115785E+307)) {
          delta1 = d2 / ((double)y->size[1] - 1.0);
          i3 = y->size[1];
          for (k = 0; k <= i3 - 3; k++) {
            y->data[k + 1] = delta1 * (1.0 + (double)k);
          }
        } else {
          delta1 = d2 / ((double)y->size[1] - 1.0);
          i3 = y->size[1];
          for (k = 0; k <= i3 - 3; k++) {
            y->data[k + 1] = (1.0 + (double)k) * delta1;
          }
        }
      }
    }
  }
}

/* End of code generation (linspace.c) */
