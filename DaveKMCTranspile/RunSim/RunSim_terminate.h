/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim_terminate.h
 *
 * Code generation for function 'RunSim_terminate'
 *
 */

#ifndef RUNSIM_TERMINATE_H
#define RUNSIM_TERMINATE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void RunSim_terminate(void);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (RunSim_terminate.h) */
