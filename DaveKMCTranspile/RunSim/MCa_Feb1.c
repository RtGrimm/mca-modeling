/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * MCa_Feb1.c
 *
 * Code generation for function 'MCa_Feb1'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "MCa_Feb1.h"
#include "RunSim_emxutil.h"
#include "rdivide_helper.h"
#include "histcounts.h"
#include "des.h"
#include "ifWhileCond.h"
#include "log.h"
#include "rand.h"
#include "flip.h"
#include "linspace.h"
#include <stdio.h>

/* Function Definitions */
void MCa_Feb1(double randoms[10000000], double Nchromophores, double
              KexcitationList, const double MCA_experimentConfig[4], const
              double inputRates[8], emxArray_real_T *GrndPopMat, emxArray_real_T
              *SngtPopMat, emxArray_real_T *TrptPopMat, emxArray_real_T
              *timeBasis, emxArray_real_T *PhotonMat, emxArray_real_T
              *TripletNet, emxArray_real_T *SingletNet, emxArray_real_T
              *GroundNet, emxArray_real_T *StateChanges, emxArray_real_T
              *TimestepMat)
{
  emxArray_real_T *discreteTimeEdges;
  emxArray_real_T *r9;
  double pulseTime;
  double maxTime;
  double MCAbins;
  int i1;
  int idx;
  emxArray_real_T *Gstacker;
  int flipbit;
  int rngTicker;
  emxArray_real_T *Sstacker;
  emxArray_real_T *Tstacker;
  emxArray_real_T *timestepstacker;
  emxArray_real_T *counts;
  emxArray_real_T *TGCounts;
  emxArray_real_T *TLCounts;
  emxArray_real_T *SGCounts;
  emxArray_real_T *SLCounts;
  emxArray_real_T *GGCounts;
  emxArray_real_T *GLCounts;
  emxArray_real_T *PhotCounts;
  int photonticker;
  int ticker;
  int tripletLossTicker;
  int tripletGainTicker;
  int singletGainTicker;
  int singletLossTicker;
  int groundGainTicker;
  int groundLossTicker;
  int i;
  static double timeVec[2000000];
  static double groundPop[2000000];
  static double singletPop[2000000];
  static double tripletPop[2000000];
  static double tripletGainTimes[2000000];
  static double tripletLossTimes[2000000];
  static double singletGainTimes[2000000];
  static double singletLossTimes[2000000];
  static double groundGainTimes[2000000];
  emxArray_real_T *TimeBinAssignments;
  static double groundLossTimes[2000000];
  emxArray_real_T *PhotonTimeBinAssignments;
  static double photonEmit[2000000];
  emxArray_real_T *GroundGainTimeBinAssignments;
  static double timesteps[2000000];
  emxArray_real_T *GroundLossTimeBinAssignments;
  emxArray_real_T *SingletGainTimeBinAssignments;
  emxArray_real_T *SingletLossTimeBinAssignments;
  emxArray_real_T *TripletGainTimeBinAssignments;
  emxArray_real_T *TripletLossTimeBinAssignments;
  emxArray_real_T *y;
  emxArray_real_T *b_timeVec;
  int MCArep;
  double currentTime;
  double S0;
  emxArray_real_T *c_timeVec;
  double S1;
  double T1;
  double advancer;
  int step;
  int exitg1;
  double cumulRates[9];
  int i2;
  int ii_size_idx_1;
  int ii;
  boolean_T exitg2;
  signed char ii_data[1];
  double currentTimeTmp;
  int ii_size[2];
  boolean_T b_ii_data[1];
  int b_ii_size[2];
  int c_ii_size[2];
  int d_ii_size[2];
  int e_ii_size[2];
  int f_ii_size[2];
  int g_ii_size[2];
  int h_ii_size[2];
  int i_ii_size[2];
  emxInit_real_T(&discreteTimeEdges, 2);
  emxInit_real_T(&r9, 2);

  /* See accompanying testscript for example usage and plotting. */
  /* Code was built upon the Berkeley Chemistry 'kineticMC' matlab script I */
  /* found on the web by searching for 'kinetic monte carlo matlab'. */
  /* I have greatly optimized it though, and many other bespoke improvements. They seemed to have left out the time */
  /* calculation, so that is done in the manner of page 384 from Schrier's */
  /* Intr. to Comp. Phys. Chem. (also optimized to not call RNG a billion times */
  /* anymore). */
  /* I recycle the random numbers and notify the user in the console output that this has happened. */
  /* Assign variables from input vector */
  /*  MCA_experimentConfig= [pulseTime maxTime MCAbins MCAcycles]; */
  pulseTime = MCA_experimentConfig[0];
  maxTime = MCA_experimentConfig[1];
  MCAbins = MCA_experimentConfig[2];

  /* Discretization timebasis */
  linspace(MCA_experimentConfig[1], MCA_experimentConfig[2] + 1.0,
           discreteTimeEdges);

  /* 'Soak-up' time samples that happen outside of bounds (make last bin huge) */
  i1 = (int)(MCA_experimentConfig[2] + 1.0) - 1;
  discreteTimeEdges->data[i1]++;

  /* What will be used for plotting */
  linspace(MCA_experimentConfig[1], MCA_experimentConfig[2], r9);
  i1 = timeBasis->size[0];
  timeBasis->size[0] = r9->size[1];
  emxEnsureCapacity_real_T(timeBasis, i1);
  idx = r9->size[1];
  for (i1 = 0; i1 < idx; i1++) {
    timeBasis->data[i1] = r9->data[i1];
  }

  /* Init. outputs matrices (time in rows, different excitations in columns) */
  i1 = PhotonMat->size[0];
  idx = (int)MCA_experimentConfig[2];
  PhotonMat->size[0] = idx;
  emxEnsureCapacity_real_T(PhotonMat, i1);
  for (i1 = 0; i1 < idx; i1++) {
    PhotonMat->data[i1] = 0.0;
  }

  i1 = TripletNet->size[0];
  TripletNet->size[0] = idx;
  emxEnsureCapacity_real_T(TripletNet, i1);
  for (i1 = 0; i1 < idx; i1++) {
    TripletNet->data[i1] = 0.0;
  }

  i1 = SingletNet->size[0];
  SingletNet->size[0] = idx;
  emxEnsureCapacity_real_T(SingletNet, i1);
  for (i1 = 0; i1 < idx; i1++) {
    SingletNet->data[i1] = 0.0;
  }

  i1 = GroundNet->size[0];
  GroundNet->size[0] = idx;
  emxEnsureCapacity_real_T(GroundNet, i1);
  for (i1 = 0; i1 < idx; i1++) {
    GroundNet->data[i1] = 0.0;
  }

  i1 = GrndPopMat->size[0];
  GrndPopMat->size[0] = idx;
  emxEnsureCapacity_real_T(GrndPopMat, i1);
  for (i1 = 0; i1 < idx; i1++) {
    GrndPopMat->data[i1] = 0.0;
  }

  i1 = SngtPopMat->size[0];
  SngtPopMat->size[0] = idx;
  emxEnsureCapacity_real_T(SngtPopMat, i1);
  for (i1 = 0; i1 < idx; i1++) {
    SngtPopMat->data[i1] = 0.0;
  }

  i1 = TrptPopMat->size[0];
  TrptPopMat->size[0] = idx;
  emxEnsureCapacity_real_T(TrptPopMat, i1);
  for (i1 = 0; i1 < idx; i1++) {
    TrptPopMat->data[i1] = 0.0;
  }

  i1 = TimestepMat->size[0];
  TimestepMat->size[0] = idx;
  emxEnsureCapacity_real_T(TimestepMat, i1);
  for (i1 = 0; i1 < idx; i1++) {
    TimestepMat->data[i1] = 0.0;
  }

  i1 = StateChanges->size[0];
  StateChanges->size[0] = idx;
  emxEnsureCapacity_real_T(StateChanges, i1);
  for (i1 = 0; i1 < idx; i1++) {
    StateChanges->data[i1] = 0.0;
  }

  emxInit_real_T(&Gstacker, 1);

  /* Size of quantization buffer */
  /* random number use and re-use bookkeeping */
  flipbit = 0;
  rngTicker = 1;

  /* Sweep over different excitation intensities */
  /*  Set simulation parameters */
  /*  ******************************************** */
  /* Excitation (currently not swept over) */
  /* Fluorescence */
  /* ISC - triplet get */
  /* RISC - triplet decay */
  /* SS - singlet-singlet interaction */
  /* ST - singlet-triplet interaction */
  /* TT - triplet-triplet interaction */
  /* Ph - phosphorescence */
  /* SF - singlet fission */
  /* ******************************************************************** */
  /* nSteps can be set high without worry, every loop iteration checks */
  /* to see if maxtime has been exceeded, and only steps again if it */
  /* has not. Probably should be while loop */
  /* Initialize a flag */
  /* initialize quantization storage */
  i1 = Gstacker->size[0];
  Gstacker->size[0] = idx;
  emxEnsureCapacity_real_T(Gstacker, i1);
  for (i1 = 0; i1 < idx; i1++) {
    Gstacker->data[i1] = 0.0;
  }

  emxInit_real_T(&Sstacker, 1);
  i1 = Sstacker->size[0];
  Sstacker->size[0] = idx;
  emxEnsureCapacity_real_T(Sstacker, i1);
  for (i1 = 0; i1 < idx; i1++) {
    Sstacker->data[i1] = 0.0;
  }

  emxInit_real_T(&Tstacker, 1);
  i1 = Tstacker->size[0];
  Tstacker->size[0] = idx;
  emxEnsureCapacity_real_T(Tstacker, i1);
  for (i1 = 0; i1 < idx; i1++) {
    Tstacker->data[i1] = 0.0;
  }

  emxInit_real_T(&timestepstacker, 1);
  i1 = timestepstacker->size[0];
  timestepstacker->size[0] = idx;
  emxEnsureCapacity_real_T(timestepstacker, i1);
  for (i1 = 0; i1 < idx; i1++) {
    timestepstacker->data[i1] = 0.0;
  }

  emxInit_real_T(&counts, 1);

  /* Initialize things to keep track of total samples in each time bin */
  /*  (for averaging pops, or just counting state changes and photons) */
  i1 = counts->size[0];
  counts->size[0] = idx;
  emxEnsureCapacity_real_T(counts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    counts->data[i1] = 0.0;
  }

  emxInit_real_T(&TGCounts, 1);
  i1 = TGCounts->size[0];
  TGCounts->size[0] = idx;
  emxEnsureCapacity_real_T(TGCounts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    TGCounts->data[i1] = 0.0;
  }

  emxInit_real_T(&TLCounts, 1);
  i1 = TLCounts->size[0];
  TLCounts->size[0] = idx;
  emxEnsureCapacity_real_T(TLCounts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    TLCounts->data[i1] = 0.0;
  }

  emxInit_real_T(&SGCounts, 1);
  i1 = SGCounts->size[0];
  SGCounts->size[0] = idx;
  emxEnsureCapacity_real_T(SGCounts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    SGCounts->data[i1] = 0.0;
  }

  emxInit_real_T(&SLCounts, 1);
  i1 = SLCounts->size[0];
  SLCounts->size[0] = idx;
  emxEnsureCapacity_real_T(SLCounts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    SLCounts->data[i1] = 0.0;
  }

  emxInit_real_T(&GGCounts, 1);
  i1 = GGCounts->size[0];
  GGCounts->size[0] = idx;
  emxEnsureCapacity_real_T(GGCounts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    GGCounts->data[i1] = 0.0;
  }

  emxInit_real_T(&GLCounts, 1);
  i1 = GLCounts->size[0];
  GLCounts->size[0] = idx;
  emxEnsureCapacity_real_T(GLCounts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    GLCounts->data[i1] = 0.0;
  }

  emxInit_real_T(&PhotCounts, 1);
  i1 = PhotCounts->size[0];
  PhotCounts->size[0] = idx;
  emxEnsureCapacity_real_T(PhotCounts, i1);
  for (i1 = 0; i1 < idx; i1++) {
    PhotCounts->data[i1] = 0.0;
  }

  /* Initializations: tickers and vars for buffers within mca loop */
  photonticker = 0;
  ticker = 0;
  tripletLossTicker = 0;
  tripletGainTicker = 0;
  singletGainTicker = 0;
  singletLossTicker = 0;
  groundGainTicker = 0;
  groundLossTicker = 0;
  for (i = 0; i < 2000000; i++) {
    timeVec[i] = rtNaN;
    groundPop[i] = rtNaN;
    singletPop[i] = rtNaN;
    tripletPop[i] = rtNaN;
    tripletGainTimes[i] = rtNaN;
    tripletLossTimes[i] = rtNaN;
    singletGainTimes[i] = rtNaN;
    singletLossTimes[i] = rtNaN;
    groundGainTimes[i] = rtNaN;
    groundLossTimes[i] = rtNaN;
    photonEmit[i] = rtNaN;
    timesteps[i] = rtNaN;
  }

  /*    $$\      $$\  $$$$$$\   $$$$$$\ */
  /*    $$$\    $$$ |$$  __$$\ $$  __$$\ */
  /*    $$$$\  $$$$ |$$ /  \__|$$ /  $$ | */
  /*    $$\$$\$$ $$ |$$ |      $$$$$$$$ | */
  /*    $$ \$$$  $$ |$$ |      $$  __$$ | */
  /*    $$ |\$  /$$ |$$ |  $$\ $$ |  $$ | */
  /*    $$ | \_/ $$ |\$$$$$$  |$$ |  $$ | */
  /*    \__|     \__| \______/ \__|  \__| */
  i1 = (int)MCA_experimentConfig[3];
  emxInit_real_T(&TimeBinAssignments, 1);
  emxInit_real_T(&PhotonTimeBinAssignments, 1);
  emxInit_real_T(&GroundGainTimeBinAssignments, 1);
  emxInit_real_T(&GroundLossTimeBinAssignments, 1);
  emxInit_real_T(&SingletGainTimeBinAssignments, 1);
  emxInit_real_T(&SingletLossTimeBinAssignments, 1);
  emxInit_real_T(&TripletGainTimeBinAssignments, 1);
  emxInit_real_T(&TripletLossTimeBinAssignments, 1);
  emxInit_real_T(&y, 2);
  emxInit_real_T(&b_timeVec, 1);
  for (MCArep = 0; MCArep < i1; MCArep++) {
    /* ***************************************** */
    /* Each MCA repetition starts at time=0. */
    currentTime = 0.0;

    /*          %initialize flag for pulse edge handling */
    /*              dontRecordFlag =0; */
    /*  Set initial conditions - defining how many of each species to */
    /*  start with */
    S0 = Nchromophores;

    /* S0 population (ground) */
    S1 = 0.0;

    /* S1 population (singlet) */
    T1 = 0.0;

    /* T1 population (triplet) */
    /* Record initial conditions */
    groundPop[ticker] = Nchromophores;
    singletPop[ticker] = 0.0;
    tripletPop[ticker] = 0.0;
    timeVec[ticker] = 0.0;
    ticker++;

    /* This reuses the random numbers when they run out */
    /*  This is a good place to check if you get errors about indices */
    /*  going over a certain limit. */
    /* This first line checks that there is enough random numbers in the */
    /* random number list to get through an iteration, but it is only an */
    /* estimation and sometimes falls short. */
    if ((rngTicker > 5.0E+6 - 20.0 / maxTime) && (rngTicker > 2500000)) {
      /* Flip randoms to shake up */
      flip(randoms);
      rngTicker = 1;
      if (flipbit == 1) {
        rngTicker = 2;
      }

      flipbit++;
      if (flipbit == 2) {
        /* Having already flipped it, make new */
        /* for choosing which rxn */
        b_rand(randoms);

        /* Necessary logarithm for time advance */
        b_log(*(double (*)[5000000])&randoms[5000000]);

        /* reset rng-reuse ticker */
        flipbit = 0;
        rngTicker = 1;
      }

      printf("Randoms list flip-reset or respawn       ");
      fflush(stdout);
    }

    /* if (ticker < hopeLessThan-10000) conditional end */
    /*        _  __           __  __             _____ */
    /*       | |/ /          |  \/  |           / ____| */
    /*       | ' /   ______  | \  / |  ______  | | */
    /*       |  <   |______| | |\/| | |______| | | */
    /*       | . \           | |  | |          | |____ */
    /*       |_|\_\          |_|  |_|           \_____| */
    /*  Kinetic Monte Carlo all the way across the MCA window */
    advancer = 0.0;
    step = 0;
    do {
      exitg1 = 0;
      if ((step < 10000) && (currentTime < maxTime)) {
        /* First-order terms ---------------------------- */
        /* Turn off pulse after pulseTime over */
        if (currentTime < pulseTime) {
          /* Pulse on */
          cumulRates[0] = KexcitationList * S0;
        } else {
          /* Pulse is over */
          cumulRates[0] = 0.0;
        }

        cumulRates[1] = inputRates[0] * S1 + cumulRates[0];
        cumulRates[2] = inputRates[1] * S1 + cumulRates[1];
        cumulRates[3] = inputRates[2] * T1 + cumulRates[2];

        /* Second order terms -------------------- */
        /* Tachiya-style stoichiometry */
        cumulRates[4] = inputRates[3] * 0.5 * S1 * (S1 - 1.0) + cumulRates[3];

        /* Singlet-triplet interaction 'checks itself' since it */
        /* requires 1 of each species, therefore can't progress */
        /* with just one ingredient */
        cumulRates[5] = inputRates[4] * S1 * T1 + cumulRates[4];

        /* Tachiya-style stoichiometry */
        cumulRates[6] = inputRates[5] * 0.5 * T1 * (T1 - 1.0) + cumulRates[5];

        /* Phosphorescence first order */
        cumulRates[7] = inputRates[6] * T1 + cumulRates[6];

        /* Singlet-fission, god help us */
        if (S0 > 0.0) {
          /* Check for auxiliary ground state */
          cumulRates[8] = inputRates[7] * S1 + cumulRates[7];
        } else {
          cumulRates[8] = cumulRates[7];

          /* Cannot split with single chromophore */
        }

        /* Total size of 'dartboard' we throw dart at to pick */
        /* process */
        /* If nothing can happen then restart excitation loop (MCA */
        /* rep) REACTION-OVER CONDITION */
        if (cumulRates[8] == 0.0) {
          /* Set 'done' condition flag */
          /* End-time sample */
          groundPop[ticker] = S0;
          singletPop[ticker] = S1;
          tripletPop[ticker] = T1;
          timesteps[ticker - 1] = advancer;
          timeVec[ticker] = currentTime;

          /* Advance ticker */
          ticker++;
          exitg1 = 1;
        } else {
          /*  Choose a random value in the range of totSum (throw */
          /*  dart to choose exit from current state) */
          advancer = randoms[rngTicker - 1] * cumulRates[8];

          /*  Get the index in the cumulRates array that this */
          /*  random value is pointing at. The function 'find' will */
          /*  get the index of the first species that has a */
          /*  cumulative rate larger than the random value. It is the */
          /*  computationally limiting step. */
          idx = 0;
          ii_size_idx_1 = 1;
          ii = 0;
          exitg2 = false;
          while ((!exitg2) && (ii < 9)) {
            if (cumulRates[ii] > advancer) {
              idx = 1;
              ii_data[0] = (signed char)(ii + 1);
              exitg2 = true;
            } else {
              ii++;
            }
          }

          if (idx == 0) {
            ii_size_idx_1 = 0;
          }

          /* -------------------------------------------------------- */
          /* Keep track of time here, ala wikipedia on KMC There is */
          /* a logarithm performed 'here' on the random value, its */
          /* just done at the beginning of the script en masse. */
          /* Random time advancement: */
          advancer = -1.0 / cumulRates[8] * randoms[rngTicker + 4999999];

          /* tenatative time advancement (The 'future' at the moment) */
          currentTimeTmp = currentTime + advancer;

          /* -------------------------------------------------------------------- */
          /* Check whether straddling the pulse edge, set flag */
          if ((currentTimeTmp > pulseTime) && (currentTime < pulseTime)) {
            i = 1;
          } else {
            i = 0;
          }

          /* If asked for, display which reaction happened */
          /*  Apply the correct reaction based on the index chosen */
          ii_size[0] = 1;
          ii_size[1] = ii_size_idx_1;
          for (i2 = 0; i2 < ii_size_idx_1; i2++) {
            b_ii_data[0] = (ii_data[0] == 1);
          }

          if (ifWhileCond(b_ii_data, ii_size)) {
            /* Ground -> singlet (excitation) */
            if (i == 0) {
              /*  If legit, do something */
              S0--;
              S1++;

              /* Mark time and advance indices */
              singletGainTimes[singletGainTicker] = currentTimeTmp;
              singletGainTicker++;
              groundLossTimes[groundLossTicker] = currentTimeTmp;
              groundLossTicker++;
            } else {
              /* The Keller Correction - no absorption, simulation moved to pulse edge time */
              currentTimeTmp = pulseTime;
              advancer = pulseTime - currentTime;

              /*                          dontRecordFlag = 1; */
              /* S0 and S1 remain the same - no unphysical */
              /* absorption          */
            }
          } else {
            b_ii_size[0] = 1;
            b_ii_size[1] = ii_size_idx_1;
            for (i2 = 0; i2 < ii_size_idx_1; i2++) {
              b_ii_data[0] = (ii_data[0] == 2);
            }

            if (ifWhileCond(b_ii_data, b_ii_size)) {
              /* Singlet -> ground (photon emitted) */
              S0++;
              S1--;

              /* Mark time and advance indices */
              groundGainTimes[groundGainTicker] = currentTimeTmp;
              groundGainTicker++;
              singletLossTimes[singletLossTicker] = currentTimeTmp;
              singletLossTicker++;

              /* Mark emission time and advance photon index */
              photonEmit[photonticker] = currentTimeTmp;
              photonticker++;
            } else {
              c_ii_size[0] = 1;
              c_ii_size[1] = ii_size_idx_1;
              for (i2 = 0; i2 < ii_size_idx_1; i2++) {
                b_ii_data[0] = (ii_data[0] == 3);
              }

              if (ifWhileCond(b_ii_data, c_ii_size)) {
                /* singlet -> triplet */
                T1++;
                S1--;

                /* Mark time and advance indices */
                tripletGainTimes[tripletGainTicker] = currentTimeTmp;
                tripletGainTicker++;
                singletLossTimes[singletLossTicker] = currentTimeTmp;
                singletLossTicker++;
              } else {
                d_ii_size[0] = 1;
                d_ii_size[1] = ii_size_idx_1;
                for (i2 = 0; i2 < ii_size_idx_1; i2++) {
                  b_ii_data[0] = (ii_data[0] == 4);
                }

                if (ifWhileCond(b_ii_data, d_ii_size)) {
                  /* triplet -> ground */
                  T1--;
                  S0++;

                  /* Mark time and advance indices */
                  tripletLossTimes[tripletLossTicker] = currentTimeTmp;
                  tripletLossTicker++;
                  groundGainTimes[groundGainTicker] = currentTimeTmp;
                  groundGainTicker++;
                } else {
                  e_ii_size[0] = 1;
                  e_ii_size[1] = ii_size_idx_1;
                  for (i2 = 0; i2 < ii_size_idx_1; i2++) {
                    b_ii_data[0] = (ii_data[0] == 5);
                  }

                  if (ifWhileCond(b_ii_data, e_ii_size)) {
                    /* singlet-singlet annihilation */
                    S1 -= 2.0;
                    S0 += 2.0;

                    /* Mark time and advance indices */
                    singletLossTimes[singletLossTicker] = currentTimeTmp;
                    singletLossTimes[singletLossTicker + 1] = currentTimeTmp;
                    singletLossTicker += 2;
                    groundGainTimes[groundGainTicker] = currentTimeTmp;
                    groundGainTimes[groundGainTicker + 1] = currentTimeTmp;
                    groundGainTicker += 2;
                  } else {
                    f_ii_size[0] = 1;
                    f_ii_size[1] = ii_size_idx_1;
                    for (i2 = 0; i2 < ii_size_idx_1; i2++) {
                      b_ii_data[0] = (ii_data[0] == 6);
                    }

                    if (ifWhileCond(b_ii_data, f_ii_size)) {
                      /* singlet-triplet annihilation */
                      S1--;
                      S0++;

                      /* This can depend on mechanism like Forster or Dexter */
                      /* Mark time and advance indices */
                      singletLossTimes[singletLossTicker] = currentTimeTmp;
                      singletLossTicker++;

                      /*                      tripletLossTimes(tripletLossTicker)= currentTimeTmp; */
                      /*                      tripletLossTicker=tripletLossTicker+1; */
                      groundGainTimes[groundGainTicker] = currentTimeTmp;
                      groundGainTicker++;

                      /*                      groundGainTimes(groundGainTicker)= currentTimeTmp; */
                      /*                      groundGainTicker=groundGainTicker+1; */
                    } else {
                      g_ii_size[0] = 1;
                      g_ii_size[1] = ii_size_idx_1;
                      for (i2 = 0; i2 < ii_size_idx_1; i2++) {
                        b_ii_data[0] = (ii_data[0] == 7);
                      }

                      if (ifWhileCond(b_ii_data, g_ii_size)) {
                        /* triplet-triplet fusion */
                        T1 -= 2.0;
                        S0++;
                        S1++;

                        /* Mark time & advance tickers (lose two triplets) */
                        tripletLossTimes[tripletLossTicker] = currentTimeTmp;
                        tripletLossTimes[tripletLossTicker + 1] = currentTimeTmp;
                        tripletLossTicker += 2;

                        /* Get a singlet out of the deal */
                        singletGainTimes[singletGainTicker] = currentTimeTmp;
                        singletGainTicker++;

                        /* As well as a ground state */
                        groundGainTimes[groundGainTicker] = currentTimeTmp;
                        groundGainTicker++;
                      } else {
                        h_ii_size[0] = 1;
                        h_ii_size[1] = ii_size_idx_1;
                        for (i2 = 0; i2 < ii_size_idx_1; i2++) {
                          b_ii_data[0] = (ii_data[0] == 8);
                        }

                        if (ifWhileCond(b_ii_data, h_ii_size)) {
                          /* triplet -> ground + PHOTON */
                          T1--;
                          S0++;

                          /* Mark time and advance indices */
                          tripletLossTimes[tripletLossTicker] = currentTimeTmp;
                          tripletLossTicker++;
                          groundGainTimes[groundGainTicker] = currentTimeTmp;
                          groundGainTicker++;
                          photonEmit[photonticker] = currentTimeTmp;
                          photonticker++;
                        } else {
                          i_ii_size[0] = 1;
                          i_ii_size[1] = ii_size_idx_1;
                          for (i2 = 0; i2 < ii_size_idx_1; i2++) {
                            b_ii_data[0] = (ii_data[0] == 9);
                          }

                          if (ifWhileCond(b_ii_data, i_ii_size)) {
                            /* Singlet-fission */
                            T1 += 2.0;

                            /* Get two triplets */
                            S1--;

                            /* Lose one singlet */
                            S0--;

                            /* Lose one ground (must conserve mass!) */
                            tripletGainTimes[tripletGainTicker] = currentTimeTmp;
                            tripletGainTimes[tripletGainTicker + 1] =
                              currentTimeTmp;
                            tripletGainTicker += 2;
                            singletLossTimes[singletLossTicker] = currentTimeTmp;
                            singletLossTicker++;
                            groundLossTimes[groundLossTicker] = currentTimeTmp;
                            groundLossTicker++;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          /*  End/Finish reaction consequences */
          /* Resolve time increase because it's kosher */
          currentTime = currentTimeTmp;

          /*                  if dontRecordFlag ==1 */
          /*                       %%Record consequences of reaction */
          /*                  groundPop(ticker)  = nan; */
          /*                  singletPop(ticker) = nan; */
          /*                  tripletPop(ticker) = nan; */
          /*                  timeVec(ticker) = currentTime; */
          /*                  timesteps(ticker-1) = advancer; */
          /*                       */
          /*                  else */
          /*                  %%Record consequences of reaction */
          groundPop[ticker] = S0;
          singletPop[ticker] = S1;
          tripletPop[ticker] = T1;
          timeVec[ticker] = currentTimeTmp;
          timesteps[ticker - 1] = advancer;

          /*                  end */
          /* Advance ticker */
          ticker++;

          /* having used both columns of the rng matrix, move to next */
          /* row */
          rngTicker++;

          /* conditional timelimit end */
          step++;
        }
      } else {
        /* Time's up so exit loop */
        /*  exit loop */
        exitg1 = 1;
      }
    } while (exitg1 == 0);

    /* Timestep loop end */
    /*                     _   _ */
    /*                    | | | | */
    /*       ___ _ __   __| | | | ___ __ ___   ___ */
    /*      / _ \ '_ \ / _` | | |/ / '_ ` _ \ / __| */
    /*     |  __/ | | | (_| | |   <| | | | | | (__ */
    /*      \___|_| |_|\__,_| |_|\_\_| |_| |_|\___| */
    /* Everything below here is dealing with quantization and data */
    /* storage/output. No consequences for reality of simulation (other */
    /* than if you chose a really small bitdepth at the beginning of the */
    /* script and now the quantization processes below are oversmoothing */
    /* your results) */
    /*  quantize when buffer full or when simulation over */
    if (ticker + 1 > 1500000) {
      /* Quantize/discretize the time samples */
      /* disp(['Quantizing ' num2str(photonticker) ' photons']) */
      /* Ticker offsets since they've been advanced prematurely */
      /*  Take all time samples and quantize them */
      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = ticker;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < ticker; i2++) {
        b_timeVec->data[i2] = timeVec[i2];
      }

      des(b_timeVec, discreteTimeEdges, TimeBinAssignments);
      if (1 > photonticker) {
        idx = 0;
      } else {
        idx = photonticker;
      }

      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = idx;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < idx; i2++) {
        b_timeVec->data[i2] = photonEmit[i2];
      }

      des(b_timeVec, discreteTimeEdges, PhotonTimeBinAssignments);
      if (1 > groundGainTicker) {
        idx = 0;
      } else {
        idx = groundGainTicker;
      }

      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = idx;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < idx; i2++) {
        b_timeVec->data[i2] = groundGainTimes[i2];
      }

      des(b_timeVec, discreteTimeEdges, GroundGainTimeBinAssignments);
      if (1 > groundLossTicker) {
        idx = 0;
      } else {
        idx = groundLossTicker;
      }

      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = idx;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < idx; i2++) {
        b_timeVec->data[i2] = groundLossTimes[i2];
      }

      des(b_timeVec, discreteTimeEdges, GroundLossTimeBinAssignments);
      if (1 > singletGainTicker) {
        idx = 0;
      } else {
        idx = singletGainTicker;
      }

      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = idx;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < idx; i2++) {
        b_timeVec->data[i2] = singletGainTimes[i2];
      }

      des(b_timeVec, discreteTimeEdges, SingletGainTimeBinAssignments);
      if (1 > singletLossTicker) {
        idx = 0;
      } else {
        idx = singletLossTicker;
      }

      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = idx;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < idx; i2++) {
        b_timeVec->data[i2] = singletLossTimes[i2];
      }

      des(b_timeVec, discreteTimeEdges, SingletLossTimeBinAssignments);
      if (1 > tripletGainTicker) {
        idx = 0;
      } else {
        idx = tripletGainTicker;
      }

      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = idx;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < idx; i2++) {
        b_timeVec->data[i2] = tripletGainTimes[i2];
      }

      des(b_timeVec, discreteTimeEdges, TripletGainTimeBinAssignments);
      if (1 > tripletLossTicker) {
        idx = 0;
      } else {
        idx = tripletLossTicker;
      }

      i2 = b_timeVec->size[0];
      b_timeVec->size[0] = idx;
      emxEnsureCapacity_real_T(b_timeVec, i2);
      for (i2 = 0; i2 < idx; i2++) {
        b_timeVec->data[i2] = tripletLossTimes[i2];
      }

      des(b_timeVec, discreteTimeEdges, TripletLossTimeBinAssignments);

      /* Use those discretization assignments to build a new variable */
      /* that sums samples together based on where they appear in time */
      /* Each ticker needs its own for-loop */
      for (idx = 0; idx < ticker; idx++) {
        /*                  try */
        Gstacker->data[(int)TimeBinAssignments->data[idx] - 1] += groundPop[idx];
        Sstacker->data[(int)TimeBinAssignments->data[idx] - 1] += singletPop[idx];
        Tstacker->data[(int)TimeBinAssignments->data[idx] - 1] += tripletPop[idx];
        timestepstacker->data[(int)TimeBinAssignments->data[idx] - 1] +=
          timesteps[idx];

        /*                  catch */
        /*                      ticker */
        /*                  end */
      }

      /* Necessary assignment for-loop:  'for ind = 1:useTicker' */
      /* Keep track of number lumped together for later reckoning */
      /* For discrete counts, this number IS the data, not just an */
      /* averaging factor, as it is for the direct population estimates. */
      /* Samples in each time bin  */
      currentTimeTmp = floor(((MCAbins + 1.0) - 0.5) + 0.5);
      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      S0 = fabs(MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(TimeBinAssignments, y, r9);
      i2 = counts->size[0];
      emxEnsureCapacity_real_T(counts, i2);
      idx = counts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        counts->data[i2] += r9->data[i2];
      }

      /* Photons in each time bin */
      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(PhotonTimeBinAssignments, y, r9);
      i2 = PhotCounts->size[0];
      emxEnsureCapacity_real_T(PhotCounts, i2);
      idx = PhotCounts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        PhotCounts->data[i2] += r9->data[i2];
      }

      /* State changes in each time bin ( */
      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(GroundGainTimeBinAssignments, y, r9);
      i2 = GGCounts->size[0];
      emxEnsureCapacity_real_T(GGCounts, i2);
      idx = GGCounts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        GGCounts->data[i2] += r9->data[i2];
      }

      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(GroundLossTimeBinAssignments, y, r9);
      i2 = GLCounts->size[0];
      emxEnsureCapacity_real_T(GLCounts, i2);
      idx = GLCounts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        GLCounts->data[i2] += r9->data[i2];
      }

      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(SingletGainTimeBinAssignments, y, r9);
      i2 = SGCounts->size[0];
      emxEnsureCapacity_real_T(SGCounts, i2);
      idx = SGCounts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        SGCounts->data[i2] += r9->data[i2];
      }

      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(SingletLossTimeBinAssignments, y, r9);
      i2 = SLCounts->size[0];
      emxEnsureCapacity_real_T(SLCounts, i2);
      idx = SLCounts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        SLCounts->data[i2] += r9->data[i2];
      }

      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(TripletGainTimeBinAssignments, y, r9);
      i2 = TGCounts->size[0];
      emxEnsureCapacity_real_T(TGCounts, i2);
      idx = TGCounts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        TGCounts->data[i2] += r9->data[i2];
      }

      S1 = currentTimeTmp;
      currentTime = (0.5 + currentTimeTmp) - (MCAbins + 1.0);
      if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, S0)) {
        S1 = currentTimeTmp + 1.0;
        advancer = MCAbins + 1.0;
      } else if (currentTime > 0.0) {
        advancer = 0.5 + (currentTimeTmp - 1.0);
      } else {
        S1 = currentTimeTmp + 1.0;
        advancer = 0.5 + currentTimeTmp;
      }

      ii = (int)S1 - 1;
      i2 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ii + 1;
      emxEnsureCapacity_real_T(y, i2);
      y->data[0] = 0.5;
      if (ii + 1 > 1) {
        y->data[ii] = advancer;
        idx = ii / 2;
        for (i = 0; i <= idx - 2; i++) {
          y->data[1 + i] = 0.5 + (1.0 + (double)i);
          y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
        }

        if (idx << 1 == ii) {
          y->data[idx] = (0.5 + advancer) / 2.0;
        } else {
          y->data[idx] = 0.5 + (double)idx;
          y->data[idx + 1] = advancer - (double)idx;
        }
      }

      histcounts(TripletLossTimeBinAssignments, y, r9);
      i2 = TLCounts->size[0];
      emxEnsureCapacity_real_T(TLCounts, i2);
      idx = TLCounts->size[0];
      for (i2 = 0; i2 < idx; i2++) {
        TLCounts->data[i2] += r9->data[i2];
      }

      /*              if doneFlag==0 */
      /* Finally, reset tickers for buffer re-use */
      photonticker = 0;
      ticker = 0;
      tripletLossTicker = 0;
      tripletGainTicker = 0;
      singletGainTicker = 0;
      singletLossTicker = 0;
      groundGainTicker = 0;
      groundLossTicker = 0;
    }

    /* ticker-high OR done-donemax time for discretization condition */
  }

  /* MCA rep loop end */
  /* Quantize/discretize the time samples */
  /* disp(['Quantizing ' num2str(photonticker) ' photons' ' - done']) */
  /* Ticker offsets since they've been advanced prematurely */
  /*  Take all time samples and quantize them */
  if (1 > ticker) {
    idx = 0;
  } else {
    idx = ticker;
  }

  emxInit_real_T(&c_timeVec, 1);
  i1 = c_timeVec->size[0];
  c_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(c_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    c_timeVec->data[i1] = timeVec[i1];
  }

  des(c_timeVec, discreteTimeEdges, TimeBinAssignments);
  emxFree_real_T(&c_timeVec);
  if (1 > photonticker) {
    idx = 0;
  } else {
    idx = photonticker;
  }

  i1 = b_timeVec->size[0];
  b_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(b_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    b_timeVec->data[i1] = photonEmit[i1];
  }

  des(b_timeVec, discreteTimeEdges, PhotonTimeBinAssignments);
  if (1 > groundGainTicker) {
    idx = 0;
  } else {
    idx = groundGainTicker;
  }

  i1 = b_timeVec->size[0];
  b_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(b_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    b_timeVec->data[i1] = groundGainTimes[i1];
  }

  des(b_timeVec, discreteTimeEdges, GroundGainTimeBinAssignments);
  if (1 > groundLossTicker) {
    idx = 0;
  } else {
    idx = groundLossTicker;
  }

  i1 = b_timeVec->size[0];
  b_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(b_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    b_timeVec->data[i1] = groundLossTimes[i1];
  }

  des(b_timeVec, discreteTimeEdges, GroundLossTimeBinAssignments);
  if (1 > singletGainTicker) {
    idx = 0;
  } else {
    idx = singletGainTicker;
  }

  i1 = b_timeVec->size[0];
  b_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(b_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    b_timeVec->data[i1] = singletGainTimes[i1];
  }

  des(b_timeVec, discreteTimeEdges, SingletGainTimeBinAssignments);
  if (1 > singletLossTicker) {
    idx = 0;
  } else {
    idx = singletLossTicker;
  }

  i1 = b_timeVec->size[0];
  b_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(b_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    b_timeVec->data[i1] = singletLossTimes[i1];
  }

  des(b_timeVec, discreteTimeEdges, SingletLossTimeBinAssignments);
  if (1 > tripletGainTicker) {
    idx = 0;
  } else {
    idx = tripletGainTicker;
  }

  i1 = b_timeVec->size[0];
  b_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(b_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    b_timeVec->data[i1] = tripletGainTimes[i1];
  }

  des(b_timeVec, discreteTimeEdges, TripletGainTimeBinAssignments);
  if (1 > tripletLossTicker) {
    idx = 0;
  } else {
    idx = tripletLossTicker;
  }

  i1 = b_timeVec->size[0];
  b_timeVec->size[0] = idx;
  emxEnsureCapacity_real_T(b_timeVec, i1);
  for (i1 = 0; i1 < idx; i1++) {
    b_timeVec->data[i1] = tripletLossTimes[i1];
  }

  des(b_timeVec, discreteTimeEdges, TripletLossTimeBinAssignments);

  /* Use those discretization assignments to build a new variable */
  /* that sums samples together based on where they appear in time */
  /* Each ticker needs its own for-loop */
  emxFree_real_T(&b_timeVec);
  emxFree_real_T(&discreteTimeEdges);
  for (idx = 0; idx < ticker; idx++) {
    /*                          try */
    Gstacker->data[(int)TimeBinAssignments->data[idx] - 1] += groundPop[idx];
    Sstacker->data[(int)TimeBinAssignments->data[idx] - 1] += singletPop[idx];
    Tstacker->data[(int)TimeBinAssignments->data[idx] - 1] += tripletPop[idx];
    timestepstacker->data[(int)TimeBinAssignments->data[idx] - 1] +=
      timesteps[idx];

    /*           */
    /*           */
    /*                          catch */
    /*                              ticker */
    /*                          end */
  }

  /* Necessary assignment for-loop:  'for ind = 1:useTicker' */
  /* Keep track of number lumped together for later reckoning */
  /* For discrete counts, this number IS the data, not just an */
  /* averaging factor, as it is for the direct population estimates. */
  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(TimeBinAssignments, y, r9);
  i1 = counts->size[0];
  emxEnsureCapacity_real_T(counts, i1);
  idx = counts->size[0];
  emxFree_real_T(&TimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    counts->data[i1] += r9->data[i1];
  }

  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(PhotonTimeBinAssignments, y, r9);
  idx = PhotCounts->size[0];
  emxFree_real_T(&PhotonTimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    PhotonMat->data[i1] = PhotCounts->data[i1] + r9->data[i1];
  }

  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(GroundGainTimeBinAssignments, y, r9);
  i1 = GGCounts->size[0];
  emxEnsureCapacity_real_T(GGCounts, i1);
  idx = GGCounts->size[0];
  emxFree_real_T(&GroundGainTimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    GGCounts->data[i1] += r9->data[i1];
  }

  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(GroundLossTimeBinAssignments, y, r9);
  i1 = GLCounts->size[0];
  emxEnsureCapacity_real_T(GLCounts, i1);
  idx = GLCounts->size[0];
  emxFree_real_T(&GroundLossTimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    GLCounts->data[i1] += r9->data[i1];
  }

  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(SingletGainTimeBinAssignments, y, r9);
  i1 = SGCounts->size[0];
  emxEnsureCapacity_real_T(SGCounts, i1);
  idx = SGCounts->size[0];
  emxFree_real_T(&SingletGainTimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    SGCounts->data[i1] += r9->data[i1];
  }

  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(SingletLossTimeBinAssignments, y, r9);
  i1 = SLCounts->size[0];
  emxEnsureCapacity_real_T(SLCounts, i1);
  idx = SLCounts->size[0];
  emxFree_real_T(&SingletLossTimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    SLCounts->data[i1] += r9->data[i1];
  }

  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(TripletGainTimeBinAssignments, y, r9);
  i1 = TGCounts->size[0];
  emxEnsureCapacity_real_T(TGCounts, i1);
  idx = TGCounts->size[0];
  emxFree_real_T(&TripletGainTimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    TGCounts->data[i1] += r9->data[i1];
  }

  S1 = floor(((MCA_experimentConfig[2] + 1.0) - 0.5) + 0.5);
  advancer = 0.5 + S1;
  currentTime = (0.5 + S1) - (MCA_experimentConfig[2] + 1.0);
  if (fabs(currentTime) < 4.4408920985006262E-16 * fmax(0.5, fabs
       (MCA_experimentConfig[2] + 1.0))) {
    S1++;
    advancer = MCA_experimentConfig[2] + 1.0;
  } else if (currentTime > 0.0) {
    advancer = 0.5 + (S1 - 1.0);
  } else {
    S1++;
  }

  ii = (int)S1 - 1;
  i1 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = ii + 1;
  emxEnsureCapacity_real_T(y, i1);
  y->data[0] = 0.5;
  if (ii + 1 > 1) {
    y->data[ii] = advancer;
    idx = ii / 2;
    for (i = 0; i <= idx - 2; i++) {
      y->data[1 + i] = 0.5 + (1.0 + (double)i);
      y->data[(ii - i) - 1] = advancer - (1.0 + (double)i);
    }

    if (idx << 1 == ii) {
      y->data[idx] = (0.5 + advancer) / 2.0;
    } else {
      y->data[idx] = 0.5 + (double)idx;
      y->data[idx + 1] = advancer - (double)idx;
    }
  }

  histcounts(TripletLossTimeBinAssignments, y, r9);
  i1 = TLCounts->size[0];
  emxEnsureCapacity_real_T(TLCounts, i1);
  idx = TLCounts->size[0];
  emxFree_real_T(&y);
  emxFree_real_T(&TripletLossTimeBinAssignments);
  for (i1 = 0; i1 < idx; i1++) {
    TLCounts->data[i1] += r9->data[i1];
  }

  emxFree_real_T(&r9);

  /* Final stuff ++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  /* If no count received, make sure not to divide by zero. */
  /* We can't initialize with ones because they get added upon */
  idx = counts->size[0];
  for (i = 0; i < idx; i++) {
    if (counts->data[i] == 0.0) {
      counts->data[i] = 1.0;
    }
  }

  /* Now get average population for time bin */
  rdivide_helper(Gstacker, counts, PhotCounts);
  idx = PhotCounts->size[0];
  emxFree_real_T(&Gstacker);
  for (i1 = 0; i1 < idx; i1++) {
    GrndPopMat->data[i1] = PhotCounts->data[i1];
  }

  rdivide_helper(Sstacker, counts, PhotCounts);
  idx = PhotCounts->size[0];
  emxFree_real_T(&Sstacker);
  for (i1 = 0; i1 < idx; i1++) {
    SngtPopMat->data[i1] = PhotCounts->data[i1];
  }

  rdivide_helper(Tstacker, counts, PhotCounts);
  idx = PhotCounts->size[0];
  emxFree_real_T(&Tstacker);
  for (i1 = 0; i1 < idx; i1++) {
    TrptPopMat->data[i1] = PhotCounts->data[i1];
  }

  /* And the timesteps */
  rdivide_helper(timestepstacker, counts, PhotCounts);
  idx = PhotCounts->size[0];
  emxFree_real_T(&counts);
  emxFree_real_T(&timestepstacker);
  for (i1 = 0; i1 < idx; i1++) {
    TimestepMat->data[i1] = PhotCounts->data[i1];
  }

  emxFree_real_T(&PhotCounts);

  /* Assign final outputs */
  idx = TGCounts->size[0];
  for (i1 = 0; i1 < idx; i1++) {
    TripletNet->data[i1] = TGCounts->data[i1] - TLCounts->data[i1];
  }

  idx = SGCounts->size[0];
  for (i1 = 0; i1 < idx; i1++) {
    SingletNet->data[i1] = SGCounts->data[i1] - SLCounts->data[i1];
  }

  idx = GGCounts->size[0];
  for (i1 = 0; i1 < idx; i1++) {
    GroundNet->data[i1] = GGCounts->data[i1] - GLCounts->data[i1];
  }

  idx = TGCounts->size[0];
  for (i1 = 0; i1 < idx; i1++) {
    StateChanges->data[i1] = ((((TGCounts->data[i1] + TLCounts->data[i1]) +
      SGCounts->data[i1]) + SLCounts->data[i1]) + GGCounts->data[i1]) +
      GLCounts->data[i1];
  }

  emxFree_real_T(&GLCounts);
  emxFree_real_T(&GGCounts);
  emxFree_real_T(&SLCounts);
  emxFree_real_T(&SGCounts);
  emxFree_real_T(&TLCounts);
  emxFree_real_T(&TGCounts);

  /* Excitation sweep loop end */
  /* FUNCTION END */
}

/* End of code generation (MCa_Feb1.c) */
