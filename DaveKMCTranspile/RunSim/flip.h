/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flip.h
 *
 * Code generation for function 'flip'
 *
 */

#ifndef FLIP_H
#define FLIP_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void flip(double x[10000000]);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (flip.h) */
