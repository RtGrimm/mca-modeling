/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim_initialize.c
 *
 * Code generation for function 'RunSim_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "RunSim_initialize.h"
#include "eml_rand_mt19937ar_stateful.h"

/* Function Definitions */
void RunSim_initialize(void)
{
  rt_InitInfAndNaN(8U);
  c_eml_rand_mt19937ar_stateful_i();
}

/* End of code generation (RunSim_initialize.c) */
