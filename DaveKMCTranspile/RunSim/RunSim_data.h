/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * RunSim_data.h
 *
 * Code generation for function 'RunSim_data'
 *
 */

#ifndef RUNSIM_DATA_H
#define RUNSIM_DATA_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Variable Declarations */
extern unsigned int state[625];

#endif

/* End of code generation (RunSim_data.h) */
