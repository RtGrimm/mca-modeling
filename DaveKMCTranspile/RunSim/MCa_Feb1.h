/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * MCa_Feb1.h
 *
 * Code generation for function 'MCa_Feb1'
 *
 */

#ifndef MCA_FEB1_H
#define MCA_FEB1_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "RunSim_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void MCa_Feb1(double randoms[10000000], double Nchromophores, double
                       KexcitationList, const double MCA_experimentConfig[4],
                       const double inputRates[8], emxArray_real_T *GrndPopMat,
                       emxArray_real_T *SngtPopMat, emxArray_real_T *TrptPopMat,
                       emxArray_real_T *timeBasis, emxArray_real_T *PhotonMat,
                       emxArray_real_T *TripletNet, emxArray_real_T *SingletNet,
                       emxArray_real_T *GroundNet, emxArray_real_T *StateChanges,
                       emxArray_real_T *TimestepMat);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (MCa_Feb1.h) */
