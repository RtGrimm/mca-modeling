/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * rand.c
 *
 * Code generation for function 'rand'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "rand.h"
#include <random>
#include <iostream>
#include <c++/8/random>

void b_rand(double r[10000000])
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dis(0, 1);

  int k;
  for (k = 0; k < 10000000; k++) {
    r[k] = dis(gen);
  }
}

/* End of code generation (rand.c) */
