/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * rdivide_helper.c
 *
 * Code generation for function 'rdivide_helper'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "RunSim.h"
#include "rdivide_helper.h"
#include "RunSim_emxutil.h"

/* Function Definitions */
void rdivide_helper(const emxArray_real_T *x, const emxArray_real_T *y,
                    emxArray_real_T *z)
{
  int i5;
  int loop_ub;
  i5 = z->size[0];
  z->size[0] = x->size[0];
  emxEnsureCapacity_real_T(z, i5);
  loop_ub = x->size[0];
  for (i5 = 0; i5 < loop_ub; i5++) {
    z->data[i5] = x->data[i5] / y->data[i5];
  }
}

/* End of code generation (rdivide_helper.c) */
