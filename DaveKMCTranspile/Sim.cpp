//
// Created by ryan on 2/9/19.
//


#include <iostream>
#include "Sim.h"
#include "RunSim/RunSim.h"
#include "RunSim/RunSim_terminate.h"
#include "RunSim/RunSim_emxAPI.h"
#include "RunSim/RunSim_initialize.h"


void
Sim::Run(double Kfl, double Kisc, double Krisc, double Kss, double Kst, double Ktt, double Kph, double Ksf, double kexc,
         double desiredBinSize, double MCAcycles, double Nchromophores) {
    emxArray_real_T *photonCount_out;
    emxArray_real_T *timeBasis_out;

    emxInitArray_real_T(&photonCount_out, 1);
    emxInitArray_real_T(&timeBasis_out, 1);

    /* Call the entry-point 'RunSim'. */
    RunSim(Kfl, Kisc, Krisc, Kss, Kst, Ktt, Kph, Ksf, kexc,
           desiredBinSize, MCAcycles, Nchromophores, photonCount_out,
           timeBasis_out);


    std::vector<double> photonCount(*photonCount_out->size);
    std::vector<double> timeBasis(*timeBasis_out->size);


    for(auto i = 0; i < *photonCount_out->size; i++) {
        photonCount[i] = (*(photonCount_out->data + i));
    }

    for(auto i = 0; i < *timeBasis_out->size; i++) {
        timeBasis[i] = (*(timeBasis_out->data + i));
    }

    emxDestroyArray_real_T(timeBasis_out);
    emxDestroyArray_real_T(photonCount_out);

    PhotonCount = std::move(photonCount);
    TimeBasis = std::move(timeBasis);

}


extern "C" {
    void* CreateSim() {
        return new Sim();
    }

    void DestorySim(void* ptr) {
        auto sim = reinterpret_cast<Sim*>(ptr);
        delete sim;
    }

    void SimRun(void* ptr, double Kfl, double Kisc, double Krisc, double Kss, double Kst, double Ktt, double Kph, double Ksf, double kexc,
               double desiredBinSize, double MCAcycles, double Nchromophores) {
        reinterpret_cast<Sim*>(ptr)->Run(Kfl, Kisc, Krisc, Kss, Kst, Ktt, Kph, Ksf, kexc,
                                                desiredBinSize, MCAcycles, Nchromophores);
    }

    int GetPhotonCountLength(void* ptr) {
        return reinterpret_cast<Sim*>(ptr)->PhotonCount.size();
    }

    void CopyPhotonCount(void* ptr, double* target) {
        auto sim = reinterpret_cast<Sim*>(ptr);
        for(auto i = 0; i < sim->PhotonCount.size(); i++) {
            target[i] = sim->PhotonCount[i];
        }

    }



}