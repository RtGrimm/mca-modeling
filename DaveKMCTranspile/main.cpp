#include <utility>

#include <iostream>
#include "RunSim/RunSim.h"
#include "RunSim/RunSim_terminate.h"
#include "RunSim/RunSim_emxAPI.h"
#include "RunSim/RunSim_initialize.h"
#include <vector>
#include <chrono>
#include <functional>
#include <iostream>
#include <random>
#include <algorithm>
#include <cmath>

float TimeFunction(std::function<void ()> func) {
    auto start = std::chrono::system_clock::now();

    func();

    auto end = std::chrono::system_clock::now();

    auto dtime = (end - start);

    auto dtimeMs = std::chrono::duration_cast<std::chrono::milliseconds>(dtime);

    return dtimeMs.count();
}


std::tuple<std::vector<double>, std::vector<double >> RunSimWrap(
        double Kfl, double Kisc, double Krisc, double Kss, double Kst,
            double Ktt, double Kph, double Ksf, double kexc, double
            desiredBinSize, double MCAcycles, double Nchromophores) {
    emxArray_real_T *photonCount_out;
    emxArray_real_T *timeBasis_out;

    emxInitArray_real_T(&photonCount_out, 1);
    emxInitArray_real_T(&timeBasis_out, 1);

    /* Call the entry-point 'RunSim'. */
    RunSim(Kfl, Kisc, Krisc, Kss, Kst, Ktt, Kph, Ksf, kexc,
           desiredBinSize, MCAcycles, Nchromophores, photonCount_out,
           timeBasis_out);


    std::vector<double> photonCount(*photonCount_out->size);
    std::vector<double> timeBasis(*timeBasis_out->size);

    for(auto i = 0; i < *photonCount_out->size; i++) {
        photonCount[i] = (*(photonCount_out->data + i));
    }

    for(auto i = 0; i < *timeBasis_out->size; i++) {
        timeBasis[i] = (*(timeBasis_out->data + i));
    }


    emxDestroyArray_real_T(timeBasis_out);
    emxDestroyArray_real_T(photonCount_out);


    return std::make_tuple(std::move(photonCount), std::move(timeBasis));
}


double Rand() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(0, 1);

    return dis(gen);
}

using FitnessF = std::function<double (std::vector<double>&)>;
using RandF = std::function<double ()>;

class Member {
public:
    std::vector<double> Params;
    double Fitness = 0;


    Member(std::vector<double> params, FitnessF fitnessF, RandF randF) :
    _FitnessF(fitnessF), _RandF(randF), Params(std::move(params)) {
    }

    void ComputeFitness() {
        Fitness = _FitnessF(Params);
    }

    Member Reproduce(Member& partner, int crossoverPoint) {
        auto offspring = Member(std::vector<double>(),
                             this->_FitnessF, this->_RandF);

        for(auto i = 0; i < partner.Params.size(); i++) {
            if(i < crossoverPoint) {
                offspring.Params.push_back(this->Params[i]);
            } else {
                offspring.Params.push_back(partner.Params[i]);
            }
        }

        return offspring;
    }

    void Mutate(double prob, double strength) {
        for (double &Param : Params) {
            if(_RandF() <= prob) {
                Param += (Param * strength * (_RandF() - 0.5)*2);
            }
        }
    }

private:
    FitnessF _FitnessF;
    RandF _RandF;
};


class Population {
public:
    Population(size_t init, const std::vector<double>& initParams, FitnessF fitness, RandF randF)
    : _Fitness(std::move(fitness)), _RandF(std::move(randF)) {
        InitPop(init, initParams);
    }

    std::vector<Member> Members;

    void Evolve(double mutateProb, double mutateStrength, int crossOverPoint) {
        std::vector<Member> oldPop = std::move(Members);
        std::vector<Member> newPop;

        std::vector<Member*> pool;

        auto maxFitness = 0.0;

        for(auto& member : oldPop) {

            member.ComputeFitness();
            maxFitness = std::max(member.Fitness, maxFitness);
        }

        std::cout << maxFitness << std::endl;

        while (pool.size() < 2) {
            for(auto& member : oldPop) {


                if(_RandF() < member.Fitness / maxFitness) {
                    pool.push_back(&member);
                }
            }
        }



        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<int> dis(0, pool.size() - 1);


        for(auto i = 0; i < oldPop.size(); i++) {
            auto parent1 = pool[dis(gen)];
            auto parent2 = pool[dis(gen)];

            auto newMember = parent1->Reproduce(*parent2, crossOverPoint);

            newMember.Mutate(mutateProb, mutateStrength);

            newPop.push_back(newMember);
        }

        Members = std::move(newPop);
    }

private:
    FitnessF _Fitness;
    RandF _RandF;

    void InitPop(size_t count, const std::vector<double>& initParams) {
        for(auto i = 0; i < count; i++) {
            std::vector<double> params = initParams;

            Members.emplace_back(
                    std::move(params), this->_Fitness, this->_RandF);
            Members[Members.size() - 1].Mutate(1, 0.1);
        }
    }
};

int main() {
    std::vector<double> output;
    output.resize(168);

    double Kfl = 2e9;
    double KiscP3HT = 1e9;
    double iscMult=KiscP3HT/Kfl;
    double KriscP3HT = 1e4;
    double riscMult=KriscP3HT/Kfl;
    double higherOrderScaling=0;
    double ssMult = 0;
    double stMult = 0.01; stMult = 1;
    double ttMult=0.000001; ttMult=0.0;
    double phMult=0;
    double sfMult=0;
    double Kisc = Kfl*iscMult;
    double Krisc = Kfl*riscMult;
    double Kss = Kfl*ssMult*higherOrderScaling;
    double Kst = Kfl*stMult*higherOrderScaling;
    double Ktt = Kfl*ttMult*higherOrderScaling;
    double Kph = Kfl*phMult;
    double Ksf = Kfl*sfMult;
    double kexc = 1e5;

    auto Nchromophores=4;
    auto MCAcycles = 1e3;
    auto desiredBinSize=600e-8;

    std::vector<double> out;

    auto time = TimeFunction([&] {
        out = std::get<0>(RunSimWrap(Kfl, Kisc, Krisc, Kss, Kst, Ktt, Kph, Ksf, kexc,
               desiredBinSize, MCAcycles, Nchromophores));



    });

    FitnessF fitness = [&] (std::vector<double>& params) {
        auto out =  std::get<0>(RunSimWrap(params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8],
                   desiredBinSize, MCAcycles, Nchromophores));


        auto minElement = std::min_element(std::begin(out) + 10, std::end(out) - 20);

        return std::tanh(10.0/(*minElement));
    };

    Population population(10, {
            Kfl, Kisc, Krisc, Kss, Kst, Ktt, Kph, Ksf, kexc
    }, fitness, Rand);

    while (true) {
        population.Evolve(0.3, 0.1, 4);
    }

    return 0;
}