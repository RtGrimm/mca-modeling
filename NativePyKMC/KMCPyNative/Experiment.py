import ctypes

from scipy.interpolate import interp1d

from KMCWrap import Simulation
import numpy as np


class Experiment:
    def __init__(self):
        self.sim = None

    def RunSim(self, cycle, count, maxTime):
        self.sim = Simulation(cycle, count)
        self.sim.RunParallel(maxTime)


    def BinTrace(self, name, t):
        times = PopTimeToArray(self.sim.QueryAll(name))
        [y, _] = np.histogram(times, bins=t)

        return y

    def InterpTrace(self, name, t):
        y = np.zeros(len(t))
        count = self.sim.CycleCount()

        for i in range(0, count):
            times = PopTimeToArray(self.sim.QueryCycle(name, i))
            counts = PopCountsToArray(self.sim.QueryCycle(name, i))

            f = interp1d(times, counts, kind="previous", fill_value="extrapolate")

            y = y + f(t)

        y = y / count

        return y[0:len(t)]


def PopTimeToArray(pop, ):
    addr = int(pop.TimesData())

    return np.ctypeslib.as_array(
        (ctypes.c_double * pop.Count()).from_address(addr))


def PopCountsToArray(pop):
    addr = int(pop.CountData())

    return np.ctypeslib.as_array(
        (ctypes.c_int * pop.Count()).from_address(addr))


def TimeSpan(start, end, binWidth):
    count = round((end - start) / binWidth)
    return np.linspace(start, end, count)
