from Experiment import Experiment, TimeSpan
from KMCWrap import ElementVector, Cycle, Simulation
import matplotlib.pyplot as plt


cycle = Cycle()

num_chromphers = 2
maxTime = 5e-5
kisc = 1e9
kiscR = 1e5
kfl = 2e9
kst = 1e8
ktt = 1e5
ksf = 1e8
kexc = 1e6

S0 = cycle.MakeElement("S0", num_chromphers)
S1 = cycle.MakeElement("S1")
T1 = cycle.MakeElement("T1")
hv = cycle.MakeElement("hv")

cycle.AddReaction(kexc, ElementVector([S0]), ElementVector([S1]))
cycle.AddReaction(kisc, ElementVector([S1]), ElementVector([T1]))
cycle.AddReaction(kiscR, ElementVector([T1]), ElementVector([S0]))
cycle.AddReaction(kfl, ElementVector([S1]), ElementVector([S0, hv]))
cycle.AddReaction(kst, ElementVector([S1, T1]), ElementVector([S0, T1]))
cycle.AddReaction(ktt, ElementVector([T1, T1]), ElementVector([S0, S0]))
cycle.AddReaction(ksf, ElementVector([S0, S1]), ElementVector([T1, T1]))

experiment = Experiment()

experiment.RunSim(cycle, 10000, maxTime)

t = TimeSpan(0, maxTime, 100e-9)
y = experiment.BinTrace("hv", t)
experiment.InterpTrace("T1", t)
plt.plot(t[0:len(y)], y)
plt.show()
