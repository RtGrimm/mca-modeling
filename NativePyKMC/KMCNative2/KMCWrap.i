


%module KMCWrap
%{
#include "Sim.h"
%}



%include "Sim.h"
%include "typemaps.i"
%include "std_vector.i"
%include "std_string.i"

%template(HistoryVector) std::vector<KMC::HistoryItem>;
%template(ElementVector) std::vector<KMC::Handle<KMC::ReactionElement>>;
%template(ReactionVector) std::vector<KMC::Reaction>;