#include <utility>
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <random>
#include <cmath>
#include <tbb/tbb.h>
#include <fstream>
#include <tuple>
#include <chrono>

#include "Sim.h"

using namespace KMC;

float TimeFunction(const std::function<void ()> &func) {
    auto start = std::chrono::system_clock::now();

    func();

    auto end = std::chrono::system_clock::now();

    auto dtime = (end - start);

    auto dtimeMs = std::chrono::duration_cast<std::chrono::milliseconds>(dtime);

    return dtimeMs.count();
}



int main() {
    Cycle cycle;

    int num_chromphers = 2;
    double maxTime = 5e-5;
    double kisc = 1e9;
    double kiscR = 1e5;
    double kfl = 2e9;
    double kst = 1e8;
    double ktt = 1e5;
    double ksf = 1e8;
    double kexc = 1e6;

    auto S0 = cycle.MakeElement("S0", num_chromphers);
    auto S1 = cycle.MakeElement("S1");
    auto T1 = cycle.MakeElement("T1");
    auto hv = cycle.MakeElement("hv");

    cycle.AddReaction(kexc, {S0}, {S1});
    cycle.AddReaction(kisc, {S1}, {T1});
    cycle.AddReaction(kiscR, {T1}, {S0});
    cycle.AddReaction(kfl, {S1}, {S0, hv});
    cycle.AddReaction(kst, {S1, T1}, {S0, T1});
    cycle.AddReaction(ktt, {T1, T1}, {S0, S0});
    cycle.AddReaction(ksf, {S0, S1}, {T1, T1});

    Simulation simulation(cycle, 10000);


    std::cout << TimeFunction([&] {
        simulation.RunParallel(maxTime);
    }) << std::endl;

    std::ofstream file("outputs/output1"
    );

    for(auto i = 0; i < simulation.History["hv"].Counts.size(); i++) {
        file << simulation.History["hv"].Times[i] << "," << simulation.History["hv"].Counts[i] << std::endl;
    }

    file.close();

    return 0;
}