#pragma once

#include <utility>
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <random>
#include <cmath>
#include <tbb/tbb.h>
#include <fstream>
#include <tuple>
#include <chrono>
#include <map>

namespace KMC {



        template<class T>
        struct Handle {
            int I = 0;

            bool operator==(const Handle &rhs) const {
                return I == rhs.I;
            }

            bool operator!=(const Handle &rhs) const {
                return !(rhs == *this);
            }
        };

        template <class T>
        class Pool {
        public:
            std::vector<T> Items;

            T& Get(Handle<T> handle) {
                return Items[handle.I];
            }

            const T& Get(Handle<T> handle) const {
                return Items[handle.I];
            }

            template <class... Args>
            Handle<T> Make(Args&&... args) {
                Items.emplace_back(std::forward<Args>(args)...);
                return Handle<T> {static_cast<int>(Items.size() - 1)};
            }

        };


        struct HistoryItem {
            double Time;
            int Count;

            HistoryItem(double Time, int Count) : Time(Time), Count(Count) {}
            HistoryItem() : Time(0), Count(0) {}
        };

        struct PopulationHistory {
            std::vector<double> Times;
            std::vector<int> Counts;

            int* CountData() {
                return Counts.data();
            }

            double* TimesData() {
                return Times.data();
            }

            size_t Count() {
                return Times.size();
            }
        };


        template<class T>
        int DuplicateCount(const std::vector<T> &xs, const T &x) {
            return std::accumulate(std::begin(xs), std::end(xs), 0,
            [&](int acc, const T &y) { return acc + (x == y ? 1 : 0); }) - 1;
        }

        class ReactionElement {
            public:
            int N = 0;
            PopulationHistory History;
            std::string Name;

            ReactionElement(const char* name = "", int n = 0) : N(n), Name(name) {}

            void Update(double t, int n) {
                History.Times.push_back(t);
                History.Counts.push_back(n);
                N = n;
            }
        };

        class Reaction {
            public:
            double Rate;
            std::vector<Handle<ReactionElement>> Reactants;
            std::vector<Handle<ReactionElement>> Products;

            Reaction(double Rate, std::vector<Handle<ReactionElement>> Reactants, std::vector<Handle<ReactionElement>> Products)
            : Rate(Rate), Reactants(std::move(Reactants)), Products(std::move(Products)) {}

            Reaction() : Rate(0) {}



            double PopProduct(Pool<ReactionElement>& pool) {
                return std::accumulate(std::begin(Reactants), std::end(Reactants), 1,
                [&](int acc, Handle<ReactionElement> reactant) {
                    auto dc = DuplicateCount(Reactants, reactant);
                    auto x = (pool.Get(reactant).N - dc) * (1.0 / (static_cast<double>(dc) + 1.0));
                    return acc * x;
                });
            }

            void Run(Pool<ReactionElement>& reactantPool, double t) {
                std::for_each(std::begin(Reactants), std::end(Reactants), [&] (Handle<ReactionElement> handle) {
                    auto& element = reactantPool.Get(handle);
                    element.Update(t, element.N - 1);
                });

                std::for_each(std::begin(Products), std::end(Products), [&] (Handle<ReactionElement> handle) {
                    auto& element = reactantPool.Get(handle);
                    element.Update(t, element.N + 1);
                });
            }
        };

        class Cycle {
            std::random_device rd;
            std::mt19937 gen;
            std::uniform_real_distribution<double> dis;
            public:
            Pool<Reaction> ReactionPool;
            Pool<ReactionElement> ElementPool;
            double t = 0;


            explicit Cycle() : t(0) {
                gen = std::mt19937(rd());
                dis = std::uniform_real_distribution<double>(0, 1);
            }


            Cycle(const Cycle& other) : Cycle() {
                ReactionPool = other.ReactionPool;
                ElementPool = other.ElementPool;
                t = other.t;
            }

            Cycle& operator=(const Cycle& other) {
                ReactionPool = other.ReactionPool;
                ElementPool = other.ElementPool;
                t = other.t;
                return *this;
            }


            double Rand() {
                return dis(gen);
            }

            double DrawTime(double wtotal) {
                return std::abs( (-1.0 / wtotal) * std::log(Rand()));
            }

            Handle<Reaction> AddReaction(double Rate,
                    std::vector<Handle<ReactionElement>> Reactants,
                    std::vector<Handle<ReactionElement>> Products) {
                return ReactionPool.Make(Rate, std::move(Reactants), std::move(Products));
            }

            Handle<ReactionElement> MakeElement(const char* name, int count = 0) {
                return ElementPool.Make(name, count);
            }


            void Run(double maxTime) {
                while(t < maxTime) {
                    std::vector<double> rateList;

                    auto& Reactions = ReactionPool.Items;

                    rateList.resize(Reactions.size());

                    double wtotal = 0;

                    {
                        auto i = 0;

                        for(auto& reaction : Reactions) {
                            auto product = reaction.PopProduct(ElementPool);
                            wtotal += product * reaction.Rate;
                            rateList[i] = wtotal;

                            i++;
                        }

                    }

                    auto r = Rand() * wtotal;

                    Reaction* reaction = nullptr;

                    {
                        auto i = 0;

                        for(double rate : rateList) {
                            if(rate > r) {
                                reaction = &Reactions[i];
                                break;
                            }

                            i++;
                        }
                    }

                    if(reaction != nullptr)
                        reaction->Run(ElementPool, t);


                    auto dt = DrawTime(wtotal);
                    t += dt;
                }
            }
        };




    class Simulation {
    public:
        std::vector<Cycle> Cycles;
        std::map<std::string, PopulationHistory> History;
        std::map<std::string, std::vector<PopulationHistory>> CycleHistory;


        Simulation(const Cycle& modelCycle, size_t cycleCount) {
            Cycles.resize(cycleCount, modelCycle);
        }

        PopulationHistory* QueryAll(const char* name) {
            return &History[std::string(name)];
        }

        PopulationHistory* QueryCycle(const char* name, int i) {
            return &(CycleHistory[std::string(name)][i]);
        }

        size_t CycleCount() const {
            return Cycles.size();
        }


        void RunParallel(double maxTime) {
            for(auto& element : Cycles[0].ElementPool.Items) {
                CycleHistory[element.Name] = std::vector<PopulationHistory>(Cycles.size());
            }

            tbb::parallel_for(0, int(Cycles.size()), [&] (int i) {
                auto& cycle = Cycles[i];

                cycle.Run(maxTime);

                for(auto& element : cycle.ElementPool.Items) {
                    CycleHistory[element.Name][i] = std::move(element.History);
                }
            });

            for(auto& element : Cycles[0].ElementPool.Items) {
                auto& historyList = CycleHistory[element.Name];

                auto& population = History[element.Name];

                for(auto& historySet : historyList) {
                    population.Times.insert(std::end(population.Times),
                            std::begin(historySet.Times), std::end(historySet.Times));

                    population.Counts.insert(std::end(population.Counts),
                                            std::begin(historySet.Counts), std::end(historySet.Counts));
                }
            }
        }
    };


    void RunParallel(const Cycle& modelCycle, size_t cycleCount, double maxTime) {
        std::vector<Cycle> cycles;
        cycles.resize(cycleCount, modelCycle);

        tbb::parallel_for(0, int(cycles.size()), [&] (int i) {
            cycles[i].Run(maxTime);
        });
    }



}